using UnityEditor;
using Gear.Editor.Data;
namespace Gear.Editor
{
    public class GEditorMenus
    {
        private const string GEAR_DATA = "Gear/Data";
        private const string GEAR_UI = "Gear/UI";

        /*
        **  生成数据代码
         */
        [MenuItem(GEAR_DATA + "/Generate Code", false, 1)]
        static void GenCodeData()
        {
            DataSOExporter.ExportClass();
        }

        /*
        **  把数据打包成AssetBundle
        */
        [MenuItem(GEAR_DATA + "/Create AssetBundle", false, 2)]
        static void CreateAssetBundle()
        {
            DataSOExporter.CreateSOAssets();
        }

        /*
         *  nginx搭建资源服务器
        **  https://blog.csdn.net/zzq900503/article/details/72821081
         */
        [MenuItem(GEAR_UI + "/Export", false, 1)]
        static void Export()
        {

        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
namespace Gear.Editor
{
    public class GMenu
    {
        [MenuItem("Gear/Model/模型预处理", false, 1)]
        public static void ModelPreprocess()
        {
        }
        [MenuItem("Gear/Model/模型打包", false, 2)]
        public static void ModelBuild()
        {
        }
        [MenuItem("Gear/Model/一键更新模型", false, 3)]
        public static void ModelUpdate()
        {
        }
        [MenuItem("Gear/Tools/生成标准目录", false, 4)]
        public static void GenerateStandardFolders()
        {
            GTools.CreateProjectDirectories();
        }
        [MenuItem("Gear/Tools/移动asset测试", false, 5)]
        public static void Move()
        {
            GTools.MoveAssetTest();
        }
    }
}
﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public static class GTextEmotionAssetsCreator
{
    [MenuItem("UITools/生成表情资源", false, 10)]
    static void Create()
    {
        Object target = Selection.activeObject;
        if (target == null || target.GetType() != typeof(Texture2D))
            return;

        Texture2D sourceTex = target as Texture2D;
        //整体路径
        string filePathWithName = AssetDatabase.GetAssetPath(sourceTex);
        //带后缀的文件名
        string fileNameWithExtension = Path.GetFileName(filePathWithName);
        //不带后缀的文件名
        string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filePathWithName);
        //不带文件名的路径
        string filePath = filePathWithName.Replace(fileNameWithExtension, "");

        GTextEmotionAssets asset = AssetDatabase.LoadAssetAtPath(filePath + fileNameWithoutExtension + "Assets.asset", typeof(GTextEmotionAssets)) as GTextEmotionAssets;

        if (asset == null)
        {
            asset = ScriptableObject.CreateInstance<GTextEmotionAssets>();
            asset._MainTexture = sourceTex;
            asset._EmotionAssets = GetSpritesInfor(sourceTex);
            AssetDatabase.CreateAsset(asset, filePath + fileNameWithoutExtension + "Assets.asset");
        }
    }
    public static List<GEmotionAsset> GetSpritesInfor(Texture2D tex)
    {
        List<GEmotionAsset> emotionAssets = new List<GEmotionAsset>();
        string filePath = UnityEditor.AssetDatabase.GetAssetPath(tex);
        Object[] objects = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(filePath);
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].GetType() == typeof(Sprite))
            {
                GEmotionAsset temp = new GEmotionAsset();
                Sprite sprite = objects[i] as Sprite;
                temp.Id = i;
                temp._Sprite = sprite;
                emotionAssets.Add(temp);
            }
        }
        return emotionAssets;
    }
}

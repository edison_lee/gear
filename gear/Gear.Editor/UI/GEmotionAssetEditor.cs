using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GTextEmotionAssets))]
public class GTextEmotionAssetsEditor : Editor
{
    GTextEmotionAssets asset;
    Vector2 ve2ScorllView;
    public void OnEnable()
    {
        asset = (GTextEmotionAssets)target;
    }
    public override void OnInspectorGUI()
    {
        if (asset._EmotionAssets == null) return;
        ve2ScorllView = GUILayout.BeginScrollView(ve2ScorllView);
        GUILayout.Label("表情信息列表");
        for (int i = 0; i < asset._EmotionAssets.Count; i++)
        {
            GUILayout.Label("\n");
            GUILayout.Label("\n");
            GEmotionAsset emotionAsset = asset._EmotionAssets[i];
            EditorGUILayout.ObjectField(emotionAsset.Name, emotionAsset._Sprite, typeof(Sprite), false);
            EditorGUILayout.IntField("Id:", emotionAsset.Id);
            EditorGUILayout.LabelField("Povit:", emotionAsset.Pivot.ToString());
            EditorGUILayout.LabelField("Rect:", emotionAsset.Rect.ToString());
        }
        GUILayout.EndScrollView();
    }
}

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GSimpleList), true)]
[CanEditMultipleObjects]
public class GSimpleListEditor : Editor
{
    protected SerializedProperty _Item;
    protected SerializedProperty _Size;
    protected SerializedProperty _Spacing;
    protected SerializedProperty _IsSingleSelection;
    protected SerializedProperty _Padding;
    protected SerializedProperty _StartCorner;
    protected SerializedProperty _StartAxis;
    protected SerializedProperty _NeedTweeningEffect;

    protected virtual void OnEnable()
    {
        _Item = serializedObject.FindProperty("_Item");
        _Size = serializedObject.FindProperty("_Size");
        _Spacing = serializedObject.FindProperty("_Spacing");
        _IsSingleSelection = serializedObject.FindProperty("_IsSingleSelection");
        _Padding = serializedObject.FindProperty("_Padding");
        _StartCorner = serializedObject.FindProperty("_StartCorner");
        _StartAxis = serializedObject.FindProperty("_StartAxis");
        _NeedTweeningEffect = serializedObject.FindProperty("_NeedTweeningEffect");
    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(_Item);
        EditorGUILayout.PropertyField(_Size);
        EditorGUILayout.PropertyField(_Spacing);
        EditorGUILayout.PropertyField(_IsSingleSelection);
        EditorGUILayout.PropertyField(_Padding, true);
        EditorGUILayout.PropertyField(_StartCorner);
        EditorGUILayout.PropertyField(_StartAxis);
        EditorGUILayout.PropertyField(_NeedTweeningEffect);
        // if (GUILayout.Button("检查"))
        // {
        //     GSimpleList l = target as GSimpleList;
        // }
        serializedObject.ApplyModifiedProperties();
    }
}
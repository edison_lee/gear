
using UnityEditor;
using UnityEditor.UI;

[CustomEditor(typeof(GItem), true)]
[CanEditMultipleObjects]
public class GItemEditor : SelectableEditor
{
    private SerializedProperty _OnItemSelected;
    protected override void OnEnable()
    {
        base.OnEnable();
        _OnItemSelected = serializedObject.FindProperty("_OnItemSelected");
    }
    public override void OnInspectorGUI()
    {
        // base.OnInspectorGUI();
        serializedObject.Update();
        EditorGUILayout.PropertyField(_OnItemSelected);
        serializedObject.ApplyModifiedProperties();
    }
}
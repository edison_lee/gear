using UnityEditor.UI;
using UnityEditor;

[CustomEditor(typeof(GText), true)]
[CanEditMultipleObjects]
public class GTextEditor : TextEditor
{
    SerializedProperty _OnClickLink;
    protected override void OnEnable()
    {
        base.OnEnable();
        //_OnClickLink = serializedObject.FindProperty("_OnClickLink");
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();
        serializedObject.Update();
        //EditorGUILayout.PropertyField(_OnClickLink);
        serializedObject.ApplyModifiedProperties();
    }
}
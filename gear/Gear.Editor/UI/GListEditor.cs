
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GList), true)]
[CanEditMultipleObjects]
public class GListEditor : GSimpleListEditor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(_Item);
        EditorGUILayout.PropertyField(_Size);
        EditorGUILayout.PropertyField(_Spacing);
        EditorGUILayout.PropertyField(_IsSingleSelection);
        EditorGUILayout.PropertyField(_Padding, true);
        EditorGUILayout.PropertyField(_StartCorner);
        EditorGUILayout.PropertyField(_NeedTweeningEffect);

        // if (GUILayout.Button("检查"))
        // {
        //     GList l = target as GList;
        // }
        serializedObject.ApplyModifiedProperties();
    }
}
﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;
namespace Gear.Editor.Data
{
    public class DataSOClassMeta
    {
        public string SavePath;
        public string ClassName;
        public string BaseClassName;
        public List<string> References = new List<string>();
        public List<DataSOPropMeta> Properties = new List<DataSOPropMeta>();
        private static string TAB = "\t";
        private static string EOF = "\r\n";

        public void AddRef(string refer)
        {
            References.Add(refer);
        }
        public void AddProp(DataSOPropMeta pro)
        {
            Properties.Add(pro);
        }
        public void Generate()
        {
            var builder = new StringBuilder();

            foreach (string oneUse in References)
            {
                builder.Append("using " + oneUse + ";").Append(EOF);
            }

            string line = string.Empty;
            if (string.IsNullOrEmpty(BaseClassName))
            {
                line = "public class " + ClassName;
            }
            else
            {
                line = "public class " + ClassName + " : " + BaseClassName + "";
            }

            builder.Append(line).Append(EOF);
            builder.Append("{").Append(EOF);

            foreach (DataSOPropMeta pro in Properties)
            {
                builder.Append(TAB);
                builder.Append("public " + pro.type + " " + pro.name + ";");
                builder.Append(EOF);
            }
            builder.Append("}");
            Write(builder);
        }

        private void Write(StringBuilder builder)
        {
            if (!Directory.Exists(SavePath))
            {
                Directory.CreateDirectory(SavePath);
            }
            File.WriteAllText(SavePath + "/"+ClassName + ".cs", builder.ToString(), Encoding.UTF8);
        }
    }
}

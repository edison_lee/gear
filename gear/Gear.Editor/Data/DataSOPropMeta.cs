﻿namespace Gear.Editor.Data
{
    public class DataSOPropMeta
    {
        public string type;
        public string name;
        public object value;
        public override string ToString()
        {
            return "type=" + type + ",name=" + name + ",value=" + value.ToString();
        }
    }
}

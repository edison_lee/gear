﻿using UnityEditor;
using System.IO;
using System.Data;
using UnityEngine;
using Excel;
using System;
using System.Reflection;
using System.Collections;
using System.Text;
namespace Gear.Editor.Data
{
    public class DataSOExporter
    {
        public const string ConstanceClassName = "Constance";
        static GConfig m_Config;
        public static GConfig Config
        {
            get
            {
                if (!m_Config)
                {
                    string resName = GConfig.ConfigFileName.Split('.')[0].Substring(1);
                    m_Config = Resources.Load<GConfig>(resName);
                }
                return m_Config;
            }
        }

        public static void ExportClass()
        {
            createClassesAtPath(Config.OutputCodePath);
            createClassesAtPath(Config.OutputCodePathLocal);
            AssetDatabase.Refresh();
        }
        static int col;
        private static void createClassesAtPath(string path)
        {
            UtilityEditor.CheckAndCreateDirectory(path + "/Base", false);

            DataSOClassMeta dataMapClass = new DataSOClassMeta();
            dataMapClass.ClassName = ConstanceClassName;
            dataMapClass.BaseClassName = "ScriptableObject";
            dataMapClass.SavePath = path;
            dataMapClass.AddRef("System.Collections.Generic");
            dataMapClass.AddRef("UnityEngine");


            string[] files = Directory.GetFiles(Config.InputExcelPath, "*.xlsx", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                try
                {
                    FileInfo info = new FileInfo(file);
                    FileStream stream = info.Open(FileMode.Open, FileAccess.Read);
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    DataSet result = excelReader.AsDataSet();

                    var formats = result.Tables[1];
                    string clsName = formats.Rows[2][0].ToString();
                    int colCount = formats.Columns.Count;

                    //子类
                    DataSOClassMeta clsMeta = new DataSOClassMeta();
                    clsMeta.SavePath = path + "/Base";
                    clsMeta.BaseClassName = "ScriptableObject";
                    clsMeta.AddRef("UnityEngine");
                    clsMeta.ClassName = clsName + "Base";

                    for (col = 0; col < colCount; col++)
                    {
                        DataSOPropMeta prop = new DataSOPropMeta();
                        var cellName = formats.Rows[0][col];
                        if (cellName != null)
                        {
                            prop.name = cellName.ToString();
                        }
                        var cellType = formats.Rows[1][col];
                        if (cellType != null)
                        {
                            prop.type = cellType.ToString();
                        }
                        clsMeta.AddProp(prop);
                    }
                    clsMeta.Generate();

                    //父类，有就不在生成
                    if (!File.Exists(path + "/" + clsName + ".cs"))
                    {
                        DataSOClassMeta clsBaseMeta = new DataSOClassMeta();
                        clsBaseMeta.SavePath = path;
                        clsBaseMeta.BaseClassName = clsMeta.ClassName;
                        clsBaseMeta.AddRef("UnityEngine");
                        clsBaseMeta.ClassName = clsName;
                        clsBaseMeta.Generate();
                    }

                    DataSOPropMeta mapProperty = new DataSOPropMeta();
                    mapProperty.name = clsName + "List = new List<" + clsName + ">()";
                    mapProperty.type = "List<" + clsName + ">";
                    dataMapClass.AddProp(mapProperty);

                    excelReader.Close();
                    stream.Close();
                }
                catch (Exception e)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(e.StackTrace);
                    sb.Append("file::" + file);
                    sb.Append("colume::" + col.ToString());
                    Debug.LogError(sb.ToString());

                }
            }
            dataMapClass.Generate();
        }


        public static void CreateSOAssets()
        {
            UtilityEditor.CheckAndCreateDirectory(Config.OutputDataPathLocalAbsolute, true);
            ScriptableObject map = ScriptableObject.CreateInstance(ConstanceClassName);
            Type mapType = map.GetType();
            string[] files = Directory.GetFiles(Config.InputExcelPath, "*.xlsx", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                try
                {
                    FileInfo info = new FileInfo(file);
                    FileStream stream = info.Open(FileMode.Open, FileAccess.Read);
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    DataSet result = excelReader.AsDataSet();

                    string clsName = result.Tables[1].Rows[2][0].ToString();
                    int rowCount = result.Tables[0].Rows.Count;
                    int colCount = result.Tables[0].Columns.Count;

                    string mapFieldName = clsName + "List";
                    FieldInfo finfo = mapType.GetField(mapFieldName);
                    object val = finfo.GetValue(map);
                    IList list = val as IList;
                    //IList list = mapType.GetField(mapFieldName).GetValue(map) as IList;

                    for (int row = 1; row < rowCount; row++)
                    {
                        //Type tt = DataManager.Instance.GetType(clsName);
                        ScriptableObject asset = ScriptableObject.CreateInstance(clsName);
                        Type tt = asset.GetType();
                        string id = result.Tables[0].Rows[row][0].ToString();

                        if (string.IsNullOrEmpty(id)) continue;
                        int rid;
                        if (!int.TryParse(id, out rid)) continue;
                        if (rid <= 0) continue;

                        for (int col = 0; col < colCount; col++)
                        {
                            string name = result.Tables[1].Rows[0][col].ToString();
                            string value = result.Tables[0].Rows[row][col].ToString();
                            string type = result.Tables[1].Rows[1][col].ToString();
                            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                            {
                                FieldInfo ifo = tt.GetField(name);
                                object cvalue = System.ComponentModel.TypeDescriptor.GetConverter(ifo.FieldType).ConvertFrom(value);
                                ifo.SetValue(asset, cvalue);
                            }
                        }
                        string savePath = Config.OutputDataPathLocalAbsolute + "/" + clsName;
                        if (!Directory.Exists(savePath))
                        {
                            Directory.CreateDirectory(savePath);
                        }
                        if (asset != null)
                        {
                            list.Add(asset);
                            AssetDatabase.CreateAsset(asset, savePath + "/" + clsName + "_" + id + ".asset");
                        }
                    }
                    excelReader.Close();
                    stream.Close();
                }
                catch (Exception e)
                {
                    Debug.LogError("异常：" + file);
                }
            }

            string abName = "constance";
            string assetPath = Config.OutputDataPathLocalAbsolute + "/Constance.asset";
            AssetDatabase.CreateAsset(map, assetPath);
            AssetImporter.GetAtPath(assetPath).assetBundleName = abName;
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            AssetBundleBuild[] builds = new AssetBundleBuild[1];
            builds[0].assetNames = new string[] { assetPath };
            builds[0].assetBundleName = abName;

            string outPath = string.Empty;
            if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android)
            {
                outPath = Config.OutputABPath + "/android";
            }
            else if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS)
            {
                outPath = Config.OutputABPath + "/ios";
            }
            else if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.StandaloneWindows || EditorUserBuildSettings.activeBuildTarget == BuildTarget.StandaloneWindows64)
            {
                outPath = Config.OutputABPath + "/windows";

            }
            BuildPipeline.BuildAssetBundles(outPath, builds, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
        }
    }
}

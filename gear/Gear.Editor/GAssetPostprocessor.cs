﻿using UnityEditor;
using UnityEngine;
using System.IO;
using Gear.Editor;
using Gear;
namespace Gear.Editor
{
    public class GAssetPostprocessor : AssetPostprocessor
    {
        // Feeds a source material.
        private void OnAssignMaterialModel()
        {
            //导入资源时设置asset bundle的名称
            //AssetImporter ai = AssetImporter.GetAtPath(assetPath);
            //i.assetBundleName = xxx;
            //ai.assetBundleVariant = xxx;
        }
        // This is called after importing of any number of assets is complete(when the Assets progress bar has reached the end).
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            foreach (string str in importedAssets)
            {
                string fileName = Path.GetFileName(str);
                if (fileName.Equals("Gear.Editor.dll"))
                {
                    string dir = Application.dataPath + "/Resources";
                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);
                    string path = dir + GConfig.ConfigFileName;
                    if (!File.Exists(path))
                    {
                        GConfig soc = ScriptableObject.CreateInstance<GConfig>();
                        AssetDatabase.CreateAsset(soc, "Assets/Resources" + GConfig.ConfigFileName);
                    }
                    //GTools.CreateProjectDirectories();
                    //TODO 移动dll有问题
                    //AssetDatabase.MoveAsset(str, "Assets/Plugins/" + fileName);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }
            }
        }
        // Handler called when asset is assigned to a different asset bundle.
        protected virtual void OnPostprocessAssetbundleNameChanged()
        {

        }
        //Add this function to a subclass to get a notification when an audio clip has completed importing.
        protected virtual void OnPostprocessAudio()
        {

        }
        //Add this function to a subclass to get a notification just before a cubemap texture has completed importing.
        protected virtual void OnPostprocessCubemap()
        {

        }
        //Gets called for each GameObject that had at least one user property attached to it in the imported file.
        protected virtual void OnPostprocessGameObjectWithUserProperties()
        {
        }
        // Add this function to a subclass to get a notification when a Material asset has completed importing.
        protected virtual void OnPostprocessMaterial()
        {
        }
        // Add this function to a subclass to get a notification when a model has completed importing.
        protected virtual void OnPostprocessModel()
        {
        }
        // Add this function to a subclass to get a notification when a SpeedTree asset has completed importing.
        protected virtual void OnPostprocessSpeedTree()
        {
        }
        // Add this function to a subclass to get a notification when an texture of sprite(s) has completed importing.
        protected virtual void OnPostprocessSprites()
        {
        }
        // Add this function to a subclass to get a notification when a texture has completed importing just before.
        protected virtual void OnPostprocessTexture()
        {
        }
        //Add this function to a subclass to get a notification just before animation from a model (.fbx, .mb file etc.) is imported.
        protected virtual void OnPreprocessAnimation()
        {
        }
        //Add this function to a subclass to get a notification just before any Asset is imported.
        protected virtual void OnPreprocessAsset()
        {
        }
        //Add this function to a subclass to get a notification just before an audio clip is being imported.
        protected virtual void OnPreprocessAudio()
        {
        }
        // Add this function to a subclass to get a notification just before a model (.fbx, .mb file etc.) is imported.
        protected virtual void OnPreprocessModel()
        {
        }
        //Add this function to a subclass to get a notification just before a SpeedTree asset(.spm file) is imported.
        protected virtual void OnPreprocessSpeedTree()
        {
        }
        // Add this function to a subclass to get a notification just before the texture importer is run.
        protected virtual void OnPreprocessTexture()
        {
        }
    }
}
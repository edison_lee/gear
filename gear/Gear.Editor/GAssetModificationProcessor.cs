﻿using System;
using UnityEditor;
namespace Gear.Editor
{
    public class GAssetModificationProcessor : UnityEditor.AssetModificationProcessor
    {
        //This is called by Unity when inspecting an asset to determine if an editor should be disabled.
        protected virtual void IsOpenForEdit()
        {

        }
        // Unity calls this method when it is about to create an Asset you haven't imported (for example, .meta files).
        protected virtual void OnWillCreateAsset()
        {

        }
        // This is called by Unity when it is about to delete an asset from disk.
        protected virtual void OnWillDeleteAsset()
        {

        }
        // Unity calls this method when it is about to move an Asset on disk.
        protected virtual void OnWillMoveAsset()
        {

        }
        // This is called by Unity when it is about to write serialized assets or scene files to disk.
        protected virtual void OnWillSaveAssets()
        {

        }
    }
}

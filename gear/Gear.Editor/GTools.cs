﻿using System;
using System.IO;
using Gear;
using UnityEditor;
using UnityEngine;
namespace Gear.Editor
{
    public class GTools
    {
        /// <summary>
        /// 创建Unity项目的目录结构，开发初期的时候只用一次。
        /// Unity目录说明：
        ///     Editor
        ///     Plugins
        ///         Runtime
        ///         Editor
        ///     Prefabs
        ///     Resources
        ///     Scenes
        ///     Scripts
        ///     Shaders
        ///     Standard Assets
        ///         Sprites
        ///         Fonts
        ///     Streaming Assets
        /// </summary>
        public static void CreateProjectDirectories()
        {
            foreach(string folder in GConfig.Instance.StandardProjectFolders)
            {
                string path = Application.dataPath + Path.DirectorySeparatorChar + folder;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
            AssetDatabase.Refresh();
        }

public static void MoveAssetTest()
        {
            AssetDatabase.MoveAsset("Assets/Gear.Editor.dll","Assets/Plugins/Gear.Editor.dll");
            AssetDatabase.Refresh();
        }
    }
}

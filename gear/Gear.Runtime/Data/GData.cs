using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Gear.Runtime.Data
{
    public class GData
    {
        private GData root;
        public GData()
        {
        }

        protected virtual void InitData()
        {

        }

        public void SetData(Object data)
        {
            GData.CopyValue(data, this);
        }

        public override string ToString()
        {
            return GData.ToString(this);
        }

        public static string ToString(object data)
        {
            string result = "";
            if (data is IList || data is Array)
            {
                result += "[";
                foreach (object v in data as IList)
                {
                    result += GData.ToString(v) + "\n";
                }
                result += "]";
            }
            else if (data is GData)
            {
                Type type = data.GetType();
                FieldInfo[] fields = type.GetFields();

                foreach (FieldInfo field in fields)
                {
                    object v = field.GetValue(data);
                    result += field.Name + ":" + GData.ToString(v) + "\n";
                }
            }
            else
            {
                result = data != null ? data.ToString() : null;
            }
            return result;
        }

        public static void CopyValue(object source, object target)
        {
            foreach (FieldInfo fi in target.GetType().GetFields())
            {
                if (source.GetType().GetField(fi.Name) != null)
                {
                    fi.SetValue(target, fi.GetValue(source));
                }
            }

            if (target is GData)
                (target as GData).InitData();
        }

        public static T CreateObject<T>(object source)
        {
            T t = Activator.CreateInstance<T>();
            CopyValue(source, t);
            return t;
        }

        public static List<T> CreateList<T>(IEnumerable sourceList)
        {
            List<T> result = new List<T>();

            Type type = null;

            foreach (object node in sourceList)
            {
                if (type == null)
                {
                    type = node.GetType();
                }
                result.Add(CreateObject<T>(node));
            }
            return result;
        }

        public static Dictionary<object, T> ListToDic<T>(IEnumerable sourceList, string fieldId = "id")
        {
            Dictionary<object, T> result = new Dictionary<object, T>();

            Type type = null;
            FieldInfo idField = null;
            foreach (object node in sourceList)
            {
                if (type == null)
                {
                    type = node.GetType();
                    idField = type.GetField(fieldId);
                }
                T t = CreateObject<T>(node);
                object id = idField.GetValue(node);
                result.Add(id, t);
            }
            return result;
        }
    }

}

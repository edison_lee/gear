using UnityEngine;
using System;
using System.Collections.Generic;
using Gear.Runtime.Operation;
namespace Gear.Runtime
{
    public class GameRoot : MonoBehaviour
    {
        protected static GameRoot s_Instance;
        public static GameRoot Instance
        {
            get
            {
                if (!s_Instance)
                {
                    var go = GameObject.Find(GConst.GameRootName);
                    if (!go)
                        go = new GameObject(GConst.GameRootName);
                    s_Instance = go.GetComponent<GameRoot>();
                    if (!s_Instance)
                        s_Instance = go.AddComponent<GameRoot>();
                }
                return s_Instance;
            }
        }
        private readonly List<Action<float>> m_Tickers = new List<Action<float>>();
        private readonly GQueue m_GlobalQueue = new GQueue(null, true);
        protected void Awake()
        {
            DontDestroyOnLoad(this);
        }

        protected void Update()
        {

        }

        protected void OnApplicationQuit()
        {

        }

        public void AddTicker(Action<float> ticker)
        {
            if (m_Tickers.Contains(ticker))
            {
                Debug.LogWarning("the ticker exist!");
                return;
            }
            m_Tickers.Add(ticker);
        }
        public void RemoveTicker(Action<float> ticker)
        {
            if (!m_Tickers.Contains(ticker))
            {
                Debug.LogWarning("the ticker not exist!");
                return;
            }
            m_Tickers.Remove(ticker);
        }
    }
}


// android 互相调用代码
/*
    void Awake()
    {
        Instance = this;
        GameObject.DontDestroyOnLoad(gameObject);
        bool.TryParse(PlayerPrefs.GetString(LOCAL_HASLOGIN), out HasEverLogin);
#if UNITY_EDITOR || UNITY_STANDALONE
        OnPlatform("_");
#else
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
        jo.Call("onFirstUnityStart");
#endif
    }

    private bool IsPlatformInservice(string platform)
    {
        return platform.Equals("gikoo");
    }
    public void OnPlatform(string str)
    {
        string[] vars = str.Split('_');
        string uid = vars[0];
        string platform = vars[1];

        //被平台唤起
        // if (!string.IsNullOrEmpty(uid) && !string.IsNullOrEmpty(platform) && !uid.Equals("null") && !platform.Equals("null") && IsPlatformInservice(platform))
        if (!string.IsNullOrEmpty(uid) && !string.IsNullOrEmpty(platform) && !uid.Equals("null"))
        {
            new RpcUserLogin(uid, "", result =>
            {
                PlayerPrefs.SetString(LOCAL_ACCOUNT, uid);
                PlayerPrefs.SetString(LOCAL_HASLOGIN, "true");
                IsOpendByPlatform = true;
                Home();
            })
            .OnError(msg =>
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
                jo.Call("showTip", msg);
                Application.Quit();
            }).Execute();
        }
        //正常登陆
        else
        {
            List<DataRanking> list = new List<DataRanking>();
            Action<string, string> t = onLogin;
            ManagerUI.Instance.Open<PanelLogin>(t);
        }
    }
 */
﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
namespace Gear.Runtime.Operation
{
    public class GOperationLoadAssetBundle : GOperationSync<AssetBundle>
    {
        private readonly string m_AssetPath;
        public GOperationLoadAssetBundle(string assetPath, Action onComplete, Action<AssetBundle> onResult, Action<string> onError = null)
            : base(onComplete, onResult, onError)
        {
            m_AssetPath = assetPath;
        }

        protected override IEnumerator ExecuteSync()
        {
            UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(GConfig.Instance.CdnUrl + m_AssetPath, 0);
            yield return request.SendWebRequest();
            if (request.isHttpError || request.isNetworkError)
            {
                Debug.LogError(request.error);
                OnError(request.error);
            }
            else if (request.isDone)
            {
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
                OnResult(bundle);
                Complete();
            }
        }
    }
}

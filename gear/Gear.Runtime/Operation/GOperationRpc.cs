﻿using System;
using UnityEngine;
using System.Collections;
using System.Reflection;
using Gear.Runtime.Data;
namespace Gear.Runtime.Operation
{
    public class RpcBase<S,R> : GOperationSync<R> where S : GData
    {
        public string modeule = "";
        public string funcName = "";
        public string method { get { return modeule + "/" + funcName + ".do"; } }
        public S sendData;
        public R recieveData;
        public RpcBase(Action onComplete)
            : base(onComplete, null, null)
        { }
        private WWWForm GetWWWForm(GData data)
        {
            WWWForm form = new WWWForm();
            Type type = data.GetType();
            FieldInfo[] infos = type.GetFields();
            foreach (FieldInfo inf in infos)
            {
                form.AddField(inf.Name, inf.GetValue(data).ToString());
            }
            return form;
        }
        protected override IEnumerator ExecuteSync()
        {   
            WWW www = new WWW(GConfig.Instance.Server + method, GetWWWForm(sendData));
            yield return www;

            string json = www.text;
 
            if (string.IsNullOrEmpty(json))
            {
                Debug.Log("the response json string is null or empty.");
                yield break;
            }

            //OnJson(json);

            //JsonData jdata = JsonMapper.ToObject(json);
            //int errorCode = int.Parse(jdata["result"].ToString());
            //// Debug.Log(errorCode);
            //if (errorCode > 0)
            //{
            //    string error = getErrorMsg(errorCode);
            //    Debug.Log(error);
            //    GRunningTip.Show(error);
            //    if (onError != null)
            //        onError(error);
            //}
            //else
            //{
            //    recieveData = JsonMapper.ToObject<T>(jdata["data"].ToJson());
            //    OnData(recieveData);
            //    if (localHandler != null)
            //    {
            //        localHandler(recieveData);
            //    }
            //}
        }
        //static List<string> errors = new List<string> { "Session expired", "Please login", "ID cannot be empty", "ID is not correct", "Login error", "System error" };
        //private string getErrorMsg(int errorId)
        //{
        //    int idx = errorId % 1000 - 1;
        //    return errors[idx];
        //}
        //protected virtual void OnJson(string json)
        //{ }
        //protected virtual void OnData(T data)
        //{ }
        //private Action<string> onError;
        //public RpcBase<T> OnError(Action<string> onerror)
        //{
        //    onError = onerror;
        //    return this;
        //}
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gear.Runtime.Operation
{
    /// <summary>播放AudioClip</summary>
    public class GOperationPlayAudio : GOperationSync<object>
    {
        private static readonly Dictionary<string, AudioSource> AudioMapPool = new Dictionary<string, AudioSource>();

        private AudioSource m_Souce;
        private bool m_AutoDestroy;
        private int m_LoopTime;
        private int m_CurrentLoopTime = 0;
        public GOperationPlayAudio(string clipName, bool single = true, float volum = 1f, int loopTime = 1, bool autoDestroy = true, Action onComplete = null, Action<object> onResult = null, Action<string> onError = null)
            : base(onComplete, onResult, onError)
        {
            if (single)
            {
                if (!AudioMapPool.TryGetValue(clipName, out m_Souce))
                {
                    m_Souce = createSource(clipName);
                    AudioMapPool.Add(clipName, m_Souce);
                }
            }
            else
            {
                m_Souce = createSource(clipName);
            }
            m_Souce.volume = volum;
            m_Souce.loop = false;
            this.m_LoopTime = loopTime;

            if (single) autoDestroy = false;
            this.m_AutoDestroy = autoDestroy;
        }

        protected override IEnumerator ExecuteSync()
        {
            if (m_Souce.isPlaying)
                m_Souce.Stop();
            m_Souce.Play();
            yield return new WaitForSeconds(m_Souce.clip.length);

            if (m_LoopTime > 0)
            {
                m_CurrentLoopTime++;
                if (m_CurrentLoopTime == m_LoopTime)
                {
                    if (m_AutoDestroy)
                        Destroy();
                    Complete();
                    yield break;
                }
            }
            Execute();
        }

        public void Stop()
        {
            m_Souce.Stop();
        }

        public override void Destroy()
        {
            base.Destroy();
            UnityEngine.Object.Destroy(m_Souce.gameObject);
        }

        private AudioSource createSource(string clipName)
        {
            GameObject obj = new GameObject("audio_" + clipName);
            obj.transform.SetParent(GameRoot.Instance.transform, false);
            AudioClip clip = Resources.Load("Audio/" + clipName) as AudioClip;
            AudioSource src = obj.AddComponent<AudioSource>();
            src.clip = clip;
            return src;
        }
    }
}
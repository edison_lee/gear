using System;
using System.Collections;

namespace Gear.Runtime.Operation
{
    public abstract class GOperationSync<R> : GOperation<R>
    {
        public GOperationSync(Action onComplete, Action<R> onResult = null, Action<string> onError = null) : base(onComplete, onResult, onError) { }

        public override void Execute()
        {
            GameRoot.Instance.StartCoroutine(ExecuteSync());
        }

        protected abstract IEnumerator ExecuteSync();
    }
}
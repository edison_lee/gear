using System;
using System.Collections.Generic;

namespace Gear.Runtime.Operation
{
    /// <summary>操作队列</summary>
    public class GQueue : GOperation<object>
    {
        private readonly Queue<IOperation> m_Queue;
        private IOperation m_CurrentOperation = null;
        private readonly bool m_IsAutoRun;

        public GQueue(List<IOperation> operations, bool isAuto, Action onComplete = null, Action<object> onResult = null, Action<string> onError = null)
            : base(onComplete, onResult, onError)
        {
            int count = operations == null ? 0 : operations.Count;
            if (count > 0)
                m_Queue = new Queue<IOperation>(operations);
            else
                m_Queue = new Queue<IOperation>();
            m_IsAutoRun = isAuto;
            if (isAuto)
                AutoExecute();
        }

        private void AutoExecute()
        {
            Execute();
        }

        public override void Execute()
        {
            if (m_CurrentOperation != null)
                m_CurrentOperation.Destroy();

            if (m_Queue.Count > 0)
            {
                m_CurrentOperation = m_Queue.Dequeue();
                if (m_CurrentOperation != null)
                {
                    m_CurrentOperation.AddCompleteHandler(AutoExecute);
                    m_CurrentOperation.Execute();
                }
            }
            else
            {
                Complete();
            }
        }
        public void Push(IOperation opera)
        {
            m_Queue.Enqueue(opera);
            if (m_IsAutoRun)
                AutoExecute();
        }

        public override void Destroy()
        {
            base.Destroy();
            if (m_CurrentOperation != null)
                m_CurrentOperation.Destroy();
            while (m_Queue.Count > 0)
                m_Queue.Dequeue().Destroy();
        }
    }
}
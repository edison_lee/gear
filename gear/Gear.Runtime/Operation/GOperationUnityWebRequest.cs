using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

/*
GlobalConfig
Description
Defines global paramters for network library.

The NetworkManager has a GlobalConfig instance ( NetworkManager.globalConfig ) that is used to configure the network when started by the NetworkManager.

Some of the attributes of this instance can be modified from the NetworkManager inspector UI in the editor.

Properties
ConnectionReadyForSend	Defines the callback delegate which you can use to get a notification when a connection is ready to send data.
MaxHosts	Defines how many hosts you can use. Default Value = 16. Max value = 128.
MaxNetSimulatorTimeout	Deprecated. Defines maximum delay for network simulator. See Also: MaxTimerTimeout.
MaxPacketSize	Defines maximum possible packet size in bytes for all network connections.
MaxTimerTimeout	Defines the maximum timeout in milliseconds for any configuration. The default value is 12 seconds (12000ms).
MinNetSimulatorTimeout	Deprecated. Defines the minimal timeout for network simulator. You cannot set up any delay less than this value. See Also: MinTimerTimeout.
MinTimerTimeout	Defines the minimum timeout in milliseconds recognised by the system. The default value is 1 ms.
NetworkEventAvailable	Defines the callback delegate which you can use to get a notification when the host (defined by hostID) has a network event. The callback is called for all event types except NetworkEventType.Nothing.See Also: NetworkEventType
ReactorMaximumReceivedMessages	This property determines the initial size of the queue that holds messages received by Unity Multiplayer before they are processed.
ReactorMaximumSentMessages	Defines the initial size of the send queue. Messages are placed in this queue ready to be sent in packets to their destination.
ReactorModel	Defines reactor model for the network library.
ThreadAwakeTimeout	Defines (1) for select reactor, minimum time period, when system will check if there are any messages for send (2) for fixrate reactor, minimum interval of time, when system will check for sending and receiving messages.
ThreadPoolSize	Defines how many worker threads are available to handle incoming and outgoing messages.
Constructors
GlobalConfig	Create new global config object.
 */
namespace Gear.Runtime.Operation
{
    public class GOperationUnityWebRequest<R> : GOperationSync<R>
    {
        private readonly string m_Url;
        private readonly string m_Method;
        private Action<float> m_OnProgress;
        public GOperationUnityWebRequest(string url, string method, Action<R> onResult = null, Action<float> onProgress = null, Action onComplete = null, Action<string> onError = null)
        : base(onComplete, onResult, onError)
        {
            m_Url = url;
            m_Method = method;
            m_OnProgress = onProgress;
        }
        protected override IEnumerator ExecuteSync()
        {
            // UnityWebRequest r = new UnityWebRequest()
            UnityWebRequest request = new UnityWebRequest(m_Url, m_Method);
            yield return request.Send();

            if (request.isHttpError || request.isNetworkError)
            {
                OnError(request.error);
                yield break;
            }

            switch (request.responseCode)
            {
                case 200:
                    Debug.Log("response code : 200");
                    break;
                case 404:
                    Debug.Log("response code : 404");
                    break;
                case 500:
                    Debug.Log("response code : 500");
                    break;
            }

            while (!request.isDone)
            {
                yield return null;
                if (m_OnProgress != null)
                    m_OnProgress.Invoke(request.downloadProgress);
            }

            object result = null;
            if (typeof(R) == typeof(string))
            {
                result = DownloadHandlerBuffer.GetContent(request);
            }
            else if (typeof(R) == typeof(Texture2D))
            {
                // Texture2D tex = DownloadHandlerTexture.GetContent(request);
                // Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero, 1f);
                result = DownloadHandlerTexture.GetContent(request);
            }
            else if (typeof(R) == typeof(AudioClip))
            {
                result = DownloadHandlerAudioClip.GetContent(request);
            }
            else if (typeof(R) == typeof(MovieTexture))
            {
                result = DownloadHandlerMovieTexture.GetContent(request);
            }
            request.Abort();
            request.Dispose();
            if (result != null)
                OnResult((R)result);
            Complete();
        }
    }
}
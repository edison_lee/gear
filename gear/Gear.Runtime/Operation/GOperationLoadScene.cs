using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gear.Runtime.Operation
{
    /// <summary>加载sceneName指定的场景</summary>
    public class GOperationLoadScene : GOperationSync<object>
    {
        private readonly string m_SceneName;
        private Action<float> m_OnProgress;
        public GOperationLoadScene(string sceneName, Action<float> onProgress = null, Action onComplete = null, Action<object> onResult = null, Action<string> onError = null)
            : base(onComplete, onResult, onError)
        {
            m_SceneName = sceneName;
            m_OnProgress = onProgress;
        }
        protected override IEnumerator ExecuteSync()
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(m_SceneName);
            while (!async.isDone)
            {
                if (m_OnProgress != null)
                    m_OnProgress.Invoke(async.progress);
                yield return async;
            }
            Complete();
        }
        public override void Destroy()
        {
            base.Destroy();
            m_OnProgress = null;
        }
    }
}

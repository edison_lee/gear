using System;
using System.Collections.Generic;

namespace Gear.Runtime.Operation
{
    public abstract class GOperation<R> : IOperation
    {
        private readonly Queue<Action> m_OnCompletes = new Queue<Action>();
        private Action<R> m_OnResult;
        private Action<string> m_OnError;
        public Action<R> OnResult { get { return m_OnResult; } set { m_OnResult = value; } }
        public Action<string> OnError { get { return m_OnError; } set { m_OnError = value; } }
        public GOperation(Action onComplete = null, Action<R> onResult = null, Action<string> onError = null)
        {
            AddCompleteHandler(onComplete);
            m_OnResult = onResult;
            m_OnError = onError;
        }
        public abstract void Execute();
        public void AddCompleteHandler(Action handler)
        {
            if (handler != null)
                m_OnCompletes.Enqueue(handler);
        }
        protected virtual void Complete()
        {
            while (m_OnCompletes.Count > 0)
                m_OnCompletes.Dequeue().Invoke();
        }
        protected virtual void Result(R result)
        {
            if (m_OnResult != null)
                m_OnResult.Invoke(result);
        }
        protected virtual void Error(string message)
        {
            if (m_OnError != null)
                m_OnError.Invoke(message);
        }
        public virtual void Destroy()
        {
            m_OnCompletes.Clear();
            m_OnResult = null;
            m_OnError = null;
        }
    }
}

using System;
using UnityEngine;
using System.Collections;

namespace Gear.Runtime.Operation
{
    /// <summary>等待数秒</summary>
    public class GOperationWaitForSeconds : GOperationSync<object>
    {
        private readonly float m_Seconds;
        public GOperationWaitForSeconds(float seconds, Action onComplete = null, Action<object> onResult = null, Action<string> onError = null) : base(onComplete, onResult, onError)
        {
            m_Seconds = seconds;
        }

        protected override IEnumerator ExecuteSync()
        {
            yield return new WaitForSeconds(m_Seconds);
            Complete();
        }
    }
}
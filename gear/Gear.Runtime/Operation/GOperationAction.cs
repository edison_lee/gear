using System;

namespace Gear.Runtime.Operation
{
    /// <summary>执行一个带参的方法</summary>
    public class GOperationAction<T> : GOperation<object>
    {
        private Action<T> m_Action;
        private readonly T m_ActionParam;
        public GOperationAction(Action<T> action, T param, Action onComplete, Action<object> onResult = null, Action<string> onError = null)
            : base(onComplete, onResult, onError)
        {
            m_Action = action;
            m_ActionParam = param;
        }
        public override void Execute()
        {
            if(m_Action!=null)
                m_Action(m_ActionParam);
            Complete();
        }
        public override void Destroy()
        {
            base.Destroy();
            m_Action = null;
        }

    }
}
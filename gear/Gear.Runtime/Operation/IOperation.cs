using System;
namespace Gear.Runtime.Operation
{
    public interface IOperation
    {
        // 开始执行
        void Execute();
        void AddCompleteHandler(Action action);
        // 执行完成
        // void Complete();
        // 处理执行结果
        // void Result(object result);
        // 处理执行错误
        // void Error(string message);
        // 销毁
        void Destroy();
    }
}

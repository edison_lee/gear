﻿using DG.Tweening;
namespace Gear.Runtime.UI
{
    public class GMotionCurve : GMotionBase
    {
        public GMotionCurveModel Model { get { return _Model as GMotionCurveModel; } }
        public override void TweenStart()
        {
            base.TweenStart();
            transform.DOPath(Model.GetCurvePath(transform.position), Model._Duration)
            .SetDelay(Model._Delay)
            .SetEase(Model._EaseType)
            .OnUpdate(OnTweeUpdate)
            .OnComplete(OnTweenEnd);
        }
        public void OnTweeUpdate()
        {
            TweenUpdate();
        }
        public void OnTweenEnd()
        {
            TweenEnd();
        }
    }
}

using UnityEngine;
using System.Collections.Generic;
using System;
namespace Gear.Runtime.UI
{
    public class GMotionBlender
    {
        private Queue<GMotionModel> _Models = new Queue<GMotionModel>();
        private GameObject _Target;
        private GMotionBase _CurMotion;
        private Action<GMotionBlender> _OnComplete;

        public void SetParams(GameObject target, List<GMotionModel> models, Action<GMotionBlender> onComplete = null)
        {
            this._Target = target;

            this._Models.Clear();
            if (models != null)
            {
                for (int i = 0; i < models.Count; i++)
                {
                    if (i < models.Count - 1)
                        models[i]._NextBlendTime = models[i + 1]._BlendTime;

                    this._Models.Enqueue(models[i]);
                }
            }

            this._OnComplete = onComplete;
        }

        public void Execute()
        {
            if (this._Target == null || this._Models == null) return;

            if (_Models.Count > 0)
            {
                GMotionModel model = _Models.Dequeue();
                _CurMotion = _Target.AddComponent(model.GetComponentType()) as GMotionBase;
                if (_CurMotion != null)
                {
                    _CurMotion.OnNext = Execute;
                    _CurMotion.SetParam(model);
                    _CurMotion.TweenStart();
                }
            }
            else
            {
                End();
            }
        }
        public void End()
        {
            if (_OnComplete != null)
                _OnComplete(this);
        }
    }
}
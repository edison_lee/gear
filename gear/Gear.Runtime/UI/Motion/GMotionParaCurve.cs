﻿using DG.Tweening;
namespace Gear.Runtime.UI
{
    public class GMotionParaCurve : GMotionBase
    {
        public GMotionParaCurveModel Model { get { return _Model as GMotionParaCurveModel; } }

        public override void TweenStart()
        {
            base.TweenStart();
            transform.DOPath(Model.GetParaCurvePath(transform.position), Model._Duration)
            .SetDelay(Model._Delay)
            .SetEase(Model._EaseType)
            .OnUpdate(OnTweeUpdate)
            .OnComplete(OnTweenEnd);
        }
        public void OnTweeUpdate()
        {
            TweenUpdate();
        }
        public void OnTweenEnd()
        {
            TweenEnd();
        }
    }
}

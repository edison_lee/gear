﻿using System;
using UnityEngine;
using DG.Tweening;
namespace Gear.Runtime.UI
{
    public abstract class GMotionModel : ScriptableObject
    {
        public float _Duration;
        public float _BlendTime = 0f;
        [HideInInspector]
        public float _NextBlendTime = 1f;
        public float _Delay = 0f;
        public Ease _EaseType = Ease.Linear;
        public abstract Type GetComponentType();
    }
}
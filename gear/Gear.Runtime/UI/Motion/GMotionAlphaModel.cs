﻿using System;
namespace Gear.Runtime.UI
{
    public class GMotionAlphaModel : GMotionModel
    {
        public float _Alpha;
        public override Type GetComponentType()
        {
            return typeof(GMotionAlpha);
        }
    }
}
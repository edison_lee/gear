﻿using DG.Tweening;
namespace Gear.Runtime.UI
{
    public class GMotionScale : GMotionBase
    {
        public GMotionScaleModel Model { get { return _Model as GMotionScaleModel; } }

        public override void TweenStart()
        {
            base.TweenStart();
            transform.DOScale(Model.GetScale(transform.localScale), Model._Duration)
            .SetDelay(Model._Delay)
            .SetEase(Model._EaseType)
            .OnUpdate(OnTweeUpdate)
            .OnComplete(OnTweenEnd);
        }
        public void OnTweeUpdate()
        {
            TweenUpdate();
        }
        public void OnTweenEnd()
        {
            TweenEnd();
        }
    }
}

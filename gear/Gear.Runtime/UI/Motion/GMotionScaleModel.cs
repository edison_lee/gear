﻿using UnityEngine;
using System;
namespace Gear.Runtime.UI
{
    public class GMotionScaleModel : GMotionModel
    {

        //scale
        public float _Scale;
        public Vector3 GetScale(Vector3 origin)
        {
            return origin * _Scale;
        }
        public override Type GetComponentType()
        {
            return typeof(GMotionScale);
        }
    }
}

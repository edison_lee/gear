﻿using UnityEngine;
using UnityEngine.UI;
namespace Gear.Runtime.UI
{
    public class GMotionAlpha : GMotionBase
    {
        private bool _IsStarted = false;
        public GMotionAlphaModel Model
        {
            get
            {
                return _Model as GMotionAlphaModel;
            }
        }
        public override void TweenStart()
        {
            base.TweenStart();
            Graphic graphic = GetComponent<Graphic>();
            Color newColor = graphic.color;
            newColor.a = Model._Alpha;
            graphic.CrossFadeColor(newColor, Model._Duration, true, true);
            _IsStarted = true;
        }
        void Update()
        {
            if (_IsStarted)
            {
                TweenUpdate();
                if (Time.time - _StartTime >= Model._Duration)
                    OnTweenEnd();
            }
        }
        public void OnTweenEnd()
        {
            TweenEnd();
            _IsStarted = false;
        }
    }
}


﻿using UnityEngine;
using System;
using System.Collections;
namespace Gear.Runtime.UI
{

    public class GMotionBase : MonoBehaviour, IMotion
    {
        public Action OnNext;
        protected float _StartTime;
        public GMotionModel _Model;

        protected Hashtable _Args = new Hashtable();
        private bool _IsOver = false;
        public void SetParam(GMotionModel model)
        {
            _Model = model;
        }
        public virtual void TweenStart()
        {
            this._StartTime = Time.time;
        }
        public virtual void TweenEnd()
        {
            Destroy(this);
        }
        public void OnDestroy()
        {
            if (OnNext != null && !_IsOver)
            {
                OnNext();
                _IsOver = true;
            }
        }
        public virtual void TweenUpdate()
        {
            CheckBlend();
        }
        private void CheckBlend()
        {
            //Debug.Log("check blend : " + _Model._NextBlendTime);
            if (_Model._NextBlendTime < 0)
            {
                float run_time = Time.time - _StartTime;
                float left_time = _Model._Duration - run_time;
                if (left_time <= -_Model._NextBlendTime)
                {
                    if (OnNext != null && !_IsOver)
                    {
                        OnNext();
                        _IsOver = true;
                    }
                }
            }
        }
    }
}
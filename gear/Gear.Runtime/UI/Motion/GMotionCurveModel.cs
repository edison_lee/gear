﻿using UnityEngine;
using System.Collections.Generic;
using System;
namespace Gear.Runtime.UI
{
    public class GMotionCurveModel : GMotionModel
    {
        public enum CurveDirection
        {
            Left,
            Right
        }
        public CurveDirection _FloatingDirection = CurveDirection.Right;
        public float _Radius;
        public int _Density;
        public Vector3[] GetCurvePath(Vector3 pos)
        {
            List<Vector3> path = new List<Vector3>();
            float piece = Mathf.PI / _Density;

            for (int i = 0; i <= _Density; i++)
            {
                float offsetx = _Radius - Mathf.Cos(i * piece) * _Radius;
                float offsety = Mathf.Sin(i * piece) * _Radius;
                offsetx = _FloatingDirection == CurveDirection.Right ? offsetx : -offsetx;
                path.Add(new Vector3(pos.x + offsetx, pos.y + offsety, pos.z));
            }
            return path.ToArray();
        }
        public override Type GetComponentType()
        {
            return typeof(GMotionCurve);
        }
    }
}

using DG.Tweening;
namespace Gear.Runtime.UI
{
    public class GMotionLinear : GMotionBase
    {
        public GMotionLinearModel Model { get { return _Model as GMotionLinearModel; } }

        public override void TweenStart()
        {
            base.TweenStart();
            transform.DOMove(Model.GetDest(transform.position), Model._Duration)
            .SetDelay(Model._Delay)
            .SetEase(Model._EaseType)
            .OnUpdate(OnTweeUpdate)
            .OnComplete(OnTweenEnd);
        }
        public void OnTweeUpdate()
        {
            TweenUpdate();
        }
        public void OnTweenEnd()
        {
            TweenEnd();
        }
    }
}

using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
namespace Gear.Runtime.UI
{
    public class GBMFontAndMotionText : GBase
    {
        private static readonly ObjectPool<GameObject> _TextObjects = new ObjectPool<GameObject>(null, o => o.SetActive(false), m => Destroy(m), 10);
        private static readonly ObjectPool<GMotionBlender> _MotionBlenders = new ObjectPool<GMotionBlender>(null, o => o.SetParams(null, null, null), m => m = null, 10);
        private static Dictionary<int, GBMFontAndMotionTextModel> _ModelCache = new Dictionary<int, GBMFontAndMotionTextModel>();
        private GBMFontAndMotionTextModel _Model;
        private Color _OriginColor;

        public static string[] FontConfigFiles = null;

        static GBMFontAndMotionText()
        {
            // 避免了字符串的拼接和Lua->C#的string传递
            FontConfigFiles = new string[] {
            "MotionConfig/attack_normal",
            "MotionConfig/attack_crit",
            "MotionConfig/under_attack_crit",
            "MotionConfig/under_attack_normal",
            "MotionConfig/heal",
            "MotionConfig/hitrecoverey",
            "MotionConfig/get_coin",
            "MotionConfig/get_exp",
            "MotionConfig/get_exp_profession",
            "MotionConfig/attack_xishou",
            "MotionConfig/attack_gedang",
            "MotionConfig/under_attack_xishou",
            "MotionConfig/under_attack_gedang"
        };
        }


        public override void SetData(object data)
        {
            base.SetData(data);
            _Model = data as GBMFontAndMotionTextModel;

            if (!IsActive())
                gameObject.SetActive(true);

            RectTrans.SetParent(_Model.GetParent(), false);
            RectTrans.anchoredPosition3D = _Model.GetLocalToLocalPos();
            RectTrans.anchorMax = RectTrans.anchorMin = RectTrans.pivot = Vector2.one * 0.5f;
            RectTrans.localScale *= _Model._FontScale;

            Text txt = gameObject.GetComponent<Text>();
            if (txt == null)
                txt = gameObject.AddComponent<Text>();
            txt.raycastTarget = false;
            txt.font = _Model._Font;
            txt.fontSize = _Model._FontSize;
            txt.text = _Model._Text;
            _OriginColor = txt.color;

            ContentSizeFitter fitter = gameObject.GetComponent<ContentSizeFitter>();
            if (fitter == null)
                fitter = gameObject.AddComponent<ContentSizeFitter>();
            fitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

            Action<GMotionBlender> onComplete = (blender) =>
            {
                transform.localScale = Vector3.one;
                txt.color = _OriginColor;

                GMotionBase[] motions = GetComponents<GMotionBase>();
                for (int i = 0; i < motions.Length; i++)
                {
                    Destroy(motions[i]);
                }

                DOTween.Kill(gameObject);

                _TextObjects.Release(gameObject);
                _MotionBlenders.Release(blender);
            };

            var motionBlender = _MotionBlenders.Get();
            motionBlender.SetParams(gameObject, _Model._MotionList, onComplete);
            motionBlender.Execute();
        }

        public static void Show(string text, Vector3 pos, int configId)
        {
            if (FontConfigFiles == null)
            {
                Debug.LogWarningFormat("FontConfig is null");
                return;
            }

            if (configId >= FontConfigFiles.Length || configId < 0)
            {
                Debug.LogWarningFormat("HUD Text Config id is wrong");
                return;
            }

            GBMFontAndMotionTextModel model;
#if UNITY_EDITOR
        model = Resources.Load<GBMFontAndMotionTextModel>(FontConfigFiles[configId]);
#else
            if (!_ModelCache.TryGetValue(configId, out model))
            {
                model = Resources.Load<GBMFontAndMotionTextModel>(FontConfigFiles[configId]);
                _ModelCache.Add(configId, model);
            }
#endif
            if (model != null && !string.IsNullOrEmpty(text))
            {
                model._Text = text;
                model._Position = pos;
                GameObject obj = _TextObjects.Get();
                GBMFontAndMotionText tip = obj.GetComponent<GBMFontAndMotionText>();
                if (tip == null)
                    tip = obj.AddComponent<GBMFontAndMotionText>();
                tip.SetData(model);
            }
        }
    }





    public class GBMFontAndMotionTextModel : ScriptableObject
    {
        [HideInInspector]
        public Vector3 _Position;
        [HideInInspector]
        public string _Text;
        [HideInInspector]
        static public Transform _Root;
        [HideInInspector]
        public string _Icon;

        public bool _IsWorldPos = false;
        public Vector2 _Offset;
        public float _XRandomNum;
        public Font _Font;
        public int _FontSize = 28;
        public float _FontScale = 3;

        private static float _CanvasScaleFactor = float.NaN;

        public List<GMotionModel> _MotionList = new List<GMotionModel>();
        public bool IsValid()
        {
            return !string.IsNullOrEmpty(_Text);
        }
        public Transform GetParent()
        {
            if (_Root == null)
                _Root = GameObject.Find("UIRootCanvas/Panel_HUD").transform;
            return _Root;
        }


        public Vector3 GetLocalToLocalPos()
        {
            if (float.IsNaN(_CanvasScaleFactor))
            {
                CanvasScaler canvasScaler = GameObject.Find("UIRootCanvas").gameObject.GetComponent<CanvasScaler>();
                _CanvasScaleFactor = (Screen.width / canvasScaler.referenceResolution.x) * (1 - canvasScaler.matchWidthOrHeight) + (Screen.height / canvasScaler.referenceResolution.y) * canvasScaler.matchWidthOrHeight;
            }

            Vector2 a = RectTransformUtility.WorldToScreenPoint(Camera.main, _Position);
            Vector2 uipos = new Vector2(a.x / _CanvasScaleFactor, a.y / _CanvasScaleFactor) + _Offset;

            float num = UnityEngine.Random.Range(-_XRandomNum, _XRandomNum);
            return new Vector3(uipos.x + num, uipos.y, 0);
        }
    }

}
﻿using UnityEngine;
using System.Collections.Generic;
namespace Gear.Runtime.UI
{

    public class GItemPooller<T> where T : Component
    {
        private GameObject _Parent;
        private GameObject _Template;
        private Stack<GameObject> _Pool = new Stack<GameObject>();
        private int _EnableCount;

        public void Regist(GameObject parent, GameObject template)
        {
            _Parent = parent;
            _Template = template;
        }

        public void SetEnableCount(int enableCount)
        {
            if (_EnableCount != enableCount)
            {
                _EnableCount = enableCount;
                Refresh();
            }
        }
        private void Refresh()
        {
            int needCount = _EnableCount;
            int realCount = _Parent.transform.childCount;
            int i;
            for (i = 0; i < realCount; i++)
            {
                Transform child = _Parent.transform.GetChild(i);
                if (i < needCount)
                {
                    child.gameObject.SetActive(true);
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
            }
            for (; i < needCount; i++)
            {
                Create();
            }
        }
        private T Create()
        {
            GameObject go = Object.Instantiate<GameObject>(_Template);
            if (!go.activeSelf)
                go.SetActive(true);
            T com = go.GetComponent<T>();
            if (com == null)
            {
                com = go.AddComponent<T>();
            }
            RectTransform rectTransform = go.transform as RectTransform;
            rectTransform.SetParent(_Parent.transform, false);
            rectTransform.localScale = Vector3.one;
            rectTransform.localRotation = Quaternion.identity;
            return com;
        }
        public T Get()
        {
            GameObject go;
            if (_Pool.Count > 0)
            {
                go = _Pool.Pop();
                go.SetActive(true);
                return go.GetComponent<T>();
            }
            else
            {
                return Create();
            }
        }
        public void Release(T element)
        {
            element.gameObject.SetActive(false);
            _Pool.Push(element.gameObject);
        }
    }
}

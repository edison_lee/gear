﻿using UnityEngine;
using System.Collections.Generic;
using System;
namespace Gear.Runtime.UI
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(GLayoutAuto))]
    public class GMenuNode : GBase
    {
        public enum NodeType
        {
            Root,
            Normal,
            Leaf
        }
        public const string LUA_FUNC_INIT = "OnInitMenuNode";
        public const string LUA_FUNC_CLICK = "OnClickMenuNode";
        private const string ITEM_NAME = "_Item";
        private const string CONTAINER_NAME = "_Container";
        private const string NODE_TEMP_NAME = "_Node";

        private static GMenuNode _SingleSelect = null;
        private static GMenuNode _Opend = null;
        public GItem.ItemSelectEvent _OnNode = new GItem.ItemSelectEvent();
        private GItem _Item;//当前节点要显示的内容，具有点击相应的以及回调操作

        private GLayoutAuto _Layout;//当前节点的字节点的Container上挂的layout脚本
        private GLayoutAuto _Container;
        private RectTransform _Node;//当前节点的子节点的模板
        private int _Index = -1;
        public int Index
        {
            get { return _Index; }
            set
            {
                _Index = value;
                gameObject.name = "_Node_" + _Index;
                _Layout.Priority = Index;
            }
        }
        private NodeType _NodeType = NodeType.Normal;
        public NodeType Nodet { get { return _NodeType; } }
        private List<GMenuNode> _Nodes = new List<GMenuNode>();
        public List<GMenuNode> ChildNodes { get { return _Nodes; } }
        private GItemMediater _ItemMediater = new GItemMediater();
        public GItemMediater Mediater { get { return _ItemMediater; } }

        private bool _IsOpen = false;
        public bool IsOpen
        {
            get
            {
                return _IsOpen;
            }
        }
        public Action<GMenuNode> _OnItemInit;
        public Action<GMenuNode> _OnItemSelect;
        public Action<GMenuNode, GameObject> _OnItemButtonClick;

        protected override void Awake()
        {
            //Node settings
            _Layout = GetComponent<GLayoutAuto>();
            if (_Layout == null)
            {
                _Layout = gameObject.AddComponent<GLayoutAuto>();
                _Layout.SortedType = SortType.Priority;
            }
            _ItemMediater.Init(OnItemInit, OnItemClick, OnItemButtonClick);
            _ItemMediater.Unselectable = true;

            //Item settings
            Transform item = RectTrans.Find(ITEM_NAME);
            if (item != null)
            {
                _Item = item.GetComponent<GItem>();
                if (_Item == null)
                {
                    _Item = item.gameObject.AddComponent<GItem>();
                }
            }
            else
            {
                //如果没有Item默认当成根节点
                _NodeType = NodeType.Root;
            }

            //Node settings
            _Node = RectTrans.Find(NODE_TEMP_NAME) as RectTransform;
            if (_Node == null)
            {
                if (_NodeType == NodeType.Root)
                {
                    Debug.LogError("根节点没有子节点模板。");
                    return;
                }
                else
                {
                    _NodeType = NodeType.Leaf;
                }
            }
            else
            {
                _Node.gameObject.SetActive(false);
            }

            if (_NodeType == NodeType.Leaf)
            {
                //叶子节点没有子集
            }
            else if (_NodeType == NodeType.Normal)
            {
                CreateContainer();

            }
            else if (_NodeType == NodeType.Root)
            {
                CreateContainer();
            }
            if (_Container != null)
                _Container.gameObject.SetActive(false);
        }
        private void CreateContainer()
        {
            Transform container = RectTrans.Find(CONTAINER_NAME);
            if (container == null)
            {
                container = new GameObject(CONTAINER_NAME).transform;
                container.SetParent(RectTrans, false);
            }
            _Container = container.GetComponent<GLayoutAuto>();
            if (_Container == null)
            {
                _Container = container.gameObject.AddComponent<GLayoutAuto>();
            }
            _Container.Priority = 1;
            _Container.RectTrans.rotation = Quaternion.identity;
            _Container.RectTrans.localScale = Vector3.one;
        }
        public void Init(int index)
        {
            Index = index;
            if (_NodeType == NodeType.Root)
            {
                OnItemInit(null);
            }
            else if (_NodeType == NodeType.Normal)
            {
                _Item.Init(_ItemMediater, 0);
            }
            else if (_NodeType == NodeType.Leaf)
            {
                _Item.Init(_ItemMediater, 0);
            }
        }
        private GMenuNode Create()
        {
            GameObject go = GameObject.Instantiate<GameObject>(_Node.gameObject);
            if (!go.activeSelf)
                go.SetActive(true);
            GMenuNode com = go.GetComponent<GMenuNode>();
            if (com == null)
            {
                com = go.AddComponent<GMenuNode>();
            }
            RectTransform rectTransform = go.transform as RectTransform;
            rectTransform.SetParent(_Container.transform, false);
            rectTransform.localScale = Vector3.one;
            rectTransform.localRotation = Quaternion.identity;
            return com;
        }
        public void Open(int needCount)
        {
            GItem item = GetItem();
            if (item != null && !item.isOn)
                item.isOn = true;

            if (_NodeType == NodeType.Leaf || needCount <= 0)
                return;
            if (_Container != null)
            {
                _Container.gameObject.SetActive(false);
                if (needCount > 0)
                {
                    _Container.gameObject.SetActive(true);
                }
            }

            _Nodes.Clear();

            int i = 0;
            int childCount = _Container.transform.childCount;
            for (i = 0; i < needCount; i++)
            {
                GMenuNode node;
                if (i < childCount)
                {
                    node = _Container.transform.GetChild(i).GetComponent<GMenuNode>();
                    node.gameObject.SetActive(true);
                }
                else
                {
                    node = Create();
                }
                node.Init(i);
                _Nodes.Add(node);
            }
            if (childCount > needCount)
            {
                for (; i < childCount; i++)
                {
                    _Container.transform.GetChild(i).gameObject.SetActive(false);
                }
            }
            //if(_Opend)
            //    _Opend.Close();
            _Opend = this;
            _IsOpen = true;
            if (_OnNode != null)
                _OnNode.Invoke(_IsOpen);
        }
        public void Close()
        {
            GItem item = GetItem();
            if (item != null && item.isOn)
                item.isOn = false;
            if (!_IsOpen || _NodeType == NodeType.Root)
                return;
            int i;
            for (i = 0; i < _Nodes.Count; i++)
            {
                _Nodes[i].gameObject.SetActive(false);
            }
            _Nodes.Clear();
            if (_Container != null)
            {
                _Container.gameObject.SetActive(false);
            }
            _IsOpen = false;
            if (_OnNode != null)
                _OnNode.Invoke(_IsOpen);
        }
        public GItem GetItem()
        {
            return _Item;
        }
        public GMenuNode GetRootNode()
        {
            GMenuNode node = this;
            while (node.Nodet != NodeType.Root)
            {
                node = node.GetParentNode();
            }
            return node;
        }
        public GMenuNode GetParentNode()
        {
            GMenuNode node = null;
            Transform t = transform.parent;
            while (t != null)
            {
                node = t.GetComponent<GMenuNode>();
                if (node) break;
                t = t.parent;
            }
            return node;
        }
        private void NotifyBrothers(GMenuNode node)
        {
            // GMenuNode parentNode = GetParentNode();
            // for (int i = 0; i < parentNode.ChildNodes.Count; i++)
            // {
            //     GMenuNode brother = parentNode.ChildNodes[i];
            //     if (brother != node && brother.GetItem().isOn)
            //         brother.GetItem().isOn = false;
            // }
            if (_SingleSelect != null && _SingleSelect != this)
            {
                _SingleSelect.GetItem().isOn = false;
            }
            _SingleSelect = node;
        }
        private void OnItemInit(GItem item)
        {
            if (GetRootNode()._OnItemInit != null)
                GetRootNode()._OnItemInit(this);
        }
        private void OnItemClick(GItem item)
        {
            if (GetRootNode()._OnItemSelect != null)
                GetRootNode()._OnItemSelect(this);

            if (_NodeType == NodeType.Leaf && _Item.isOn)
            {
                NotifyBrothers(this);
            }
        }
        private void OnItemButtonClick(GItem item, GameObject btn)
        {
            if (GetRootNode()._OnItemButtonClick != null)
                GetRootNode()._OnItemButtonClick(this, btn);
        }
    }
}
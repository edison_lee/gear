﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace Gear.Runtime.UI
{
public class GListItem : GBase, IPointerClickHandler
{

    public delegate void ItemDelegate(GameObject go, int index);

    public ItemDelegate OnItemClick;

    public ItemDelegate OnItemClickButton;

    public ItemDelegate OnItemInit;
    private int index;


    protected override void Awake()
    {
        GButton[] gbtns = gameObject.GetComponentsInChildren<GButton>(true);
        for (int i = 0; i < gbtns.Length; i++)
        {
            gbtns[i].OnClick += OnItemButtonClick;
        }
        Button[] buttons = gameObject.GetComponentsInChildren<Button>(true);
        for (int i = 0; i < buttons.Length; i++)
        {
            Button btn = buttons[i];
            btn.onClick.AddListener(delegate () { OnItemButtonClick(btn.gameObject); });
        }
    }
    public void UpdateItem(int index)
    {
        this.index = index;
        this.gameObject.name = "item-" + index;
        if (this.IsActive() && OnItemInit != null)
            OnItemInit(gameObject, index);
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (OnItemClick != null)
            OnItemClick(gameObject, index);
    }

    private void OnItemButtonClick(GameObject go)
    {
        if (OnItemClickButton != null)
            OnItemClickButton(go, index);
    }
}
}
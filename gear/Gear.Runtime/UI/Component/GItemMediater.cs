using UnityEngine;
using UnityEngine.Events;
using System;
namespace Gear.Runtime.UI
{
    public class GItemMediater
    {
        public class ItemButtonClickEvent : UnityEvent<GItem, GameObject> { }
        public Action<GItem> _OnItemInit;
        public Action<GItem> _OnItemSelect;
        public Action<GItem, GameObject> _OnItemButtonClick;

        public bool Unselectable { get; set; }
        public GItemMediater() { }
        public GItemMediater(Action<GItem> onInit, Action<GItem> onSelect, Action<GItem, GameObject> onButtonClick)
        {
            Init(onInit, onSelect, onButtonClick);
        }

        public void Init(Action<GItem> onInit, Action<GItem> onSelect, Action<GItem, GameObject> onButtonClick)
        {
            _OnItemInit = onInit;
            _OnItemSelect = onSelect;
            _OnItemButtonClick = onButtonClick;
        }
    }
}
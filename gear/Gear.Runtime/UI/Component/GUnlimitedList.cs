﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
namespace Gear.Runtime.UI
{
    public class GUnlimitedList : GListBase
    {

        public enum Direction
        {
            Horizontal = 1,
            Vertical
        }
        [SerializeField]
        protected int _NumberOfRow;
        [SerializeField]
        protected int _NumberOfColumn;
        [SerializeField]
        public RectTransform _Cell;
        [SerializeField]
        protected Vector2 _Spacing = Vector2.one;
        [SerializeField]
        private Direction _Direction = Direction.Horizontal;

        private int _BufferNo = 4;
        private float _PrevPos = 0;
        private int _CurrentIndex;//页面的第一行（列）在整个conten中的位置
        private ScrollRect _ScrollRect;
        private int _Count;
        protected List<GListItem> _InstantiateItems = new List<GListItem>();
        public int CurrentIndex { get { return _CurrentIndex; } set { _CurrentIndex = value; } }
        private Vector2 Page { get { return new Vector2(_NumberOfRow, _NumberOfColumn); } }

        public Vector2 CellRect { get { return _Cell != null ? _Cell.sizeDelta : new Vector2(100, 100); } }

        public Direction CurrentDirection { get { return _Direction; } }

        public float CellScale { get { return CurrentDirection == Direction.Horizontal ? (CellRect.x + _Spacing.x) : (CellRect.y + _Spacing.y); } }

        public float DirectionPos { get { return CurrentDirection == Direction.Horizontal ? RectTrans.anchoredPosition.x : RectTrans.anchoredPosition.y; } }

        public Vector2 InstantiateSize
        {
            get
            {
                Vector2 m_InstantiateSize = Vector2.zero;
                float rows, cols;
                if (CurrentDirection == Direction.Horizontal)
                {
                    rows = Page.x;
                    cols = Page.y + (float)_BufferNo;
                }
                else
                {
                    rows = Page.x + (float)_BufferNo;
                    cols = Page.y;
                }
                m_InstantiateSize = new Vector2(rows, cols);
                return m_InstantiateSize;
            }
        }

        public int PageScale { get { return CurrentDirection == Direction.Horizontal ? (int)Page.x : (int)Page.y; } }

        public int InstantiateCount { get { return (int)InstantiateSize.x * (int)InstantiateSize.y; } }

        public float Scale { get { return CurrentDirection == Direction.Horizontal ? 1f : -1f; } }

        public float MaxPrevPos
        {
            get
            {
                float result;
                Vector2 max = getRectByNum(this._Count);
                if (CurrentDirection == Direction.Horizontal)
                {
                    result = max.y - Page.y;
                }
                else
                {
                    result = max.x - Page.x;
                }
                return result * CellScale;
            }
        }
        protected override void Awake()
        {
            base.Awake();
            _ScrollRect = GetComponentInParent<ScrollRect>();
            _ScrollRect.horizontal = CurrentDirection == Direction.Horizontal;
            _ScrollRect.vertical = CurrentDirection == Direction.Vertical;
            RectTrans.anchorMax = RectTrans.anchorMin = RectTrans.pivot = Vector2.up;
            if (_Cell != null)
                _Cell.gameObject.SetActive(false);
        }
        public void SetItemCount(int count)
        {

            if (_Cell == null)
            {
                _Cell = Trans.GetChild(0) as RectTransform;
                if (_Cell == null)
                {
                    Debug.Log("请为Item制定一个模板！");
                    return;
                }
            }

            _Cell.gameObject.SetActive(false);

            this._Count = count;
            this.UpdateBound();
            this.FitRealItemCount();
            this.ReuseFormat();
        }

        public void UpdateBound()
        {
            float bw, bh;
            Vector2 page = getRectByNum(this._Count);
            Vector2 realBound = new Vector2(page.y * (CellRect.x + _Spacing.x), page.x * (CellRect.y + _Spacing.y));
            RectTransform scrollTransform = _ScrollRect.transform as RectTransform;
            bw = realBound.x < scrollTransform.sizeDelta.x ? scrollTransform.sizeDelta.x : realBound.x;
            bh = realBound.y < scrollTransform.sizeDelta.y ? scrollTransform.sizeDelta.y : realBound.y;
            this.RectTrans.sizeDelta = new Vector2(bw, bh);
        }

        private void FitRealItemCount()
        {
            int left = this._Count > InstantiateCount ? InstantiateCount - _InstantiateItems.Count : this._Count - _InstantiateItems.Count;

            if (left > 0)
            {
                for (int i = 0; i < left; i++)
                {
                    _InstantiateItems.Add(createEmptyItem());
                }
            }
            else if (left < 0)
            {
                left = -left;
                List<GListItem> garbages = _InstantiateItems.GetRange(_InstantiateItems.Count - left, left);
                _InstantiateItems.RemoveRange(_InstantiateItems.Count - left, left);
                for (int num = 0; num < garbages.Count; num++)
                {
                    GameObject.Destroy(garbages[num].gameObject);
                }
            }
        }

        private void ReuseFormat()
        {
            for (int num = 0; num < _InstantiateItems.Count; num++)
            {
                int index = _CurrentIndex * PageScale + num;
                GListItem item = _InstantiateItems[num];
                moveItemToIndex(index, item);
            }
        }

        public void ResetListPos()
        {
            _ScrollRect.StopMovement();
            RectTrans.anchoredPosition = Vector2.zero;
        }

        public void MoveToIndex(int index)
        {
            _CurrentIndex = index;
            this.ReuseFormat();
        }
        private GListItem createEmptyItem()
        {
            RectTransform item = Object.Instantiate(_Cell) as RectTransform;
            item.SetParent(transform, false);
            item.anchorMax = Vector2.up;
            item.anchorMin = Vector2.up;
            item.pivot = Vector2.up;
            item.anchoredPosition = Vector2.zero;

            GListItem itemCon = item.GetComponent<GListItem>();
            if (itemCon == null)
                itemCon = item.gameObject.AddComponent<GListItem>();

            if (this.OnInitItem != null)
            {
                itemCon.OnItemInit = (go, index) =>
                {
                    this.OnInitItem(this.gameObject, go, index);
                };
            }

            if (this.OnSelectItem != null)
            {
                itemCon.OnItemClick = (go, index) =>
                {
                    this.OnSelectItem(this.gameObject, go, index);
                };
            }

            if (this.OnSelectItemButton != null)
            {
                itemCon.OnItemClickButton = (go, index) =>
                {
                    this.OnSelectItemButton(this.gameObject, go, index);
                };
            }
            item.gameObject.SetActive(true);
            return itemCon;
        }

        private Vector2 getRectByNum(int num)
        {
            return CurrentDirection == Direction.Horizontal ?
                new Vector2(Page.x, Mathf.CeilToInt(num / Page.x)) :
                new Vector2(Mathf.CeilToInt(num / Page.y), Page.y);

        }
        void Update()
        {
            if (_InstantiateItems.Count == 0) return;
            while (Scale * DirectionPos - _PrevPos < -CellScale * 2)
            {
                if (_PrevPos <= -MaxPrevPos) return;

                _PrevPos -= CellScale;

                List<GListItem> range = _InstantiateItems.GetRange(0, PageScale);
                _InstantiateItems.RemoveRange(0, PageScale);
                _InstantiateItems.AddRange(range);
                for (int i = 0; i < range.Count; i++)
                {
                    moveItemToIndex(_CurrentIndex * PageScale + _InstantiateItems.Count + i, range[i]);
                }
                _CurrentIndex++;
            }
            while (Scale * DirectionPos - _PrevPos > -CellScale)
            {
                if (Mathf.RoundToInt(_PrevPos) >= 0) return;

                _PrevPos += CellScale;

                _CurrentIndex--;

                if (_CurrentIndex < 0) return;

                List<GListItem> range = _InstantiateItems.GetRange(_InstantiateItems.Count - PageScale, PageScale);
                _InstantiateItems.RemoveRange(_InstantiateItems.Count - PageScale, PageScale);
                _InstantiateItems.InsertRange(0, range);
                for (int i = 0; i < range.Count; i++)
                {
                    moveItemToIndex(_CurrentIndex * PageScale + i, range[i]);
                }
            }
        }

        private void moveItemToIndex(int index, GListItem item)
        {
            if (index >= this._Count) return;
            item.RectTrans.anchoredPosition = getPosByIndex(index);
            item.gameObject.SetActive(index < this._Count);
            item.UpdateItem(index);
        }

        private Vector2 getPosByIndex(int index)
        {
            int x, y;
            if (CurrentDirection == Direction.Horizontal)
            {
                x = index % (int)Page.x;
                y = Mathf.FloorToInt(index / Page.x);
            }
            else
            {
                x = Mathf.FloorToInt(index / Page.y);
                y = index % (int)Page.y;
            }

            return new Vector2(y * Mathf.RoundToInt(CellRect.x + _Spacing.x), -x * Mathf.RoundToInt(CellRect.y + _Spacing.y));
        }
    }
}
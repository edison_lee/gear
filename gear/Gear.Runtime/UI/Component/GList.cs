using UnityEngine;
using UnityEngine.UI;
namespace Gear.Runtime.UI
{
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(ScrollRect))]
    [RequireComponent(typeof(RectMask2D))]
    public class GList : GSimpleList
    {
        private ScrollRect _ScrollRect;
        protected override void Awake()
        {
            base.Awake();
            _ScrollRect = GetComponent<ScrollRect>();
            GridLayoutGroup.Axis startAxis = GridLayoutGroup.Axis.Horizontal;
            if (_ScrollRect.vertical)
            {
                startAxis = GridLayoutGroup.Axis.Horizontal;
                _ScrollRect.horizontal = false;
            }
            else if (_ScrollRect.horizontal)
            {
                startAxis = GridLayoutGroup.Axis.Vertical;
                _ScrollRect.vertical = false;
            }
            _LayoutRoot.StartAxis = startAxis;
            _ScrollRect.content = _LayoutRoot.RectTrans;
        }
        public void SetVerticalNormalizedPosition(float value)
        {
            _ScrollRect.verticalNormalizedPosition = value;
        }
        public void SetHorizontalNormalizedPosition(float value)
        {
            _ScrollRect.horizontalNormalizedPosition = value;
        }
    }
}
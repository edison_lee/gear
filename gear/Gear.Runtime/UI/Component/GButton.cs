﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using DG.Tweening;
namespace Gear.Runtime.UI
{
    public class GButton : Selectable, IPointerClickHandler, ISubmitHandler, IButtonClickCallBack
    {
        //public class ButtonClickedEvent : UnityEvent { }
        //private ButtonClickedEvent _OnClick = new ButtonClickedEvent();

        public delegate void OnButtonClick(GameObject go);
        public OnButtonClick OnClick, OnSelected, PointerDownHandler, PointerUpHandler, OnPointerEnterHandler, OnPointerExitHandler;

        private bool _Selected = false;//当前选中状态
        private Text _Text;
        [SerializeField]
        private bool _Selectable = false;//是否支持选中
        [SerializeField]
        private bool _AlwaysCallSelect = false;//选中和取消选中是不是都需要调用OnSelected回调
        [SerializeField]
        private bool _IsProfessionModel = false;
        public bool IsProfessionModel { get { return _IsProfessionModel; } }


        protected Rect _ORect = new Rect(0, 0, 0, 0);
        protected Camera UICamera;
        private Vector3[] _WorldCorners = new Vector3[4];

        private Transform _Transform;
        private RectTransform _RectTransform;

        public Transform Trans
        {
            get
            {
                if (_Transform == null)
                    _Transform = transform;
                return _Transform;
            }
        }

        public RectTransform RectTrans
        {
            get
            {
                if (_RectTransform == null)
                    _RectTransform = Trans as RectTransform;
                if (_RectTransform == null)
                    _RectTransform = gameObject.AddComponent<RectTransform>();
                return _RectTransform;
            }
        }
        public string text
        {
            get
            {
                string result = "";
                if (_Text != null)
                    result = _Text.text;
                return result;
            }
            set
            {
                if (_Text != null)
                {
                    _Text.text = value;
                }
            }
        }
        public bool Selectable { get { return _Selectable; } set { _Selectable = value; } }
        public bool Selected { get { return _Selected; } }
        protected GButton() { }

        #region sysytem functions and button basic logic
        protected override void Awake()
        {
            _Transform = transform;
            _Text = GetComponentInChildren<Text>();
            if (targetGraphic == null)
                targetGraphic = GetComponent<Image>() as Graphic;
        }
        protected override void Start()
        {
            UICamera = GameObject.Find("/UICamera").GetComponent<Camera>();
        }
        protected override void OnEnable()
        {
            // to be override 
        }
        protected override void OnDisable()
        {
            //to be override
        }
        protected override void OnDestroy()
        {
            //to be override
        }
        public virtual void OnPointerClick(PointerEventData eventData)
        {
            //取消，在PointerUp中处理
            // if (!interactable) return;
            // OnButtonClickHandler(this.gameObject);
        }
        public virtual void OnSubmit(BaseEventData eventData)
        {
            if (!interactable) return;
            //不确定，待修改
            OnButtonClickHandler(this.gameObject);
        }
        public override void OnPointerDown(PointerEventData eventData)
        {
            if (!interactable) return;

            _ORect = GetCurrentRect();
            if (_Selectable)
            {
                _Selected = !_Selected;
                if (_Selected)
                {
                    OnButtonSelectHandler(this.gameObject);
                    ButtonSelectEffect();
                }
                else
                {
                    if (_AlwaysCallSelect)
                        OnButtonSelectHandler(this.gameObject);
                    ButtonDeselectEffect();
                }
            }
            base.OnPointerDown(eventData);
            ButtonDownEffect();
            if (PointerDownHandler != null)
                PointerDownHandler(gameObject);
        }
        public void TransToNormalWithoutCall()
        {
            _Selected = false;
            ButtonDeselectEffect();
        }

        public void TransToSelectedWithoutCall()
        {
            _Selected = true;
            ButtonSelectEffect();
        }
        public override void OnPointerUp(PointerEventData eventData)
        {
            if (!interactable) return;
            if (_Selectable) return;
            base.OnPointerUp(eventData);
            if (UICamera != null && _ORect.Contains(UICamera.ScreenToWorldPoint(eventData.position)))
            {
                OnButtonClickHandler(gameObject);
            }
            ButtonUpEffect();
            if (PointerUpHandler != null)
                PointerUpHandler(gameObject);
        }
        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (!interactable) return;
            if (_Selectable) return;
            base.OnPointerEnter(eventData);
            ButtonEnterEffect();

            if (OnPointerEnterHandler != null)
                OnPointerEnterHandler(gameObject);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            if (!interactable) return;
            if (_Selectable) return;
            base.OnPointerExit(eventData);
            ButtonExitEffect();

            if (OnPointerExitHandler != null)
                OnPointerExitHandler(gameObject);
        }
        #endregion
        public virtual void OnButtonClickHandler(GameObject obj)
        {
            if (_Selectable) return;
            if (OnClick != null)
                OnClick(obj);
            //AudioManager.Instance.Play("button_click");
        }

        public virtual void OnButtonSelectHandler(GameObject value)
        {
            if (!_Selectable) return;
            if (OnSelected != null)
                OnSelected(value);
        }
        protected Rect GetCurrentRect()
        {
            RectTrans.GetWorldCorners(_WorldCorners);
            return new Rect(_WorldCorners[0].x, _WorldCorners[0].y, _WorldCorners[2].x - _WorldCorners[0].x, _WorldCorners[2].y - _WorldCorners[0].y);
        }
        public virtual void ButtonDownEffect() { }
        public virtual void ButtonUpEffect() { }
        public virtual void ButtonEnterEffect() { }
        public virtual void ButtonExitEffect() { }
        public virtual void ButtonSelectEffect() { }
        public virtual void ButtonDeselectEffect() { }

        // object类型的参数虽然灵活，但是很容易出现装箱拆箱操作，效率不高，故禁用
        //     public virtual void SetData(object data)
        //     {
        //         _Data = data;
        //     }
    }




    public class GButtonColor : GButton
    {

        private Graphic[] _ChildGraphics;

        [SerializeField]
        private Color _HightLightColor = new Color32(0, 0, 0, 50);
        [SerializeField]
        private bool _UseAlpha = false;
        protected override void Awake()
        {
            base.Awake();
            _ChildGraphics = GetComponentsInChildren<Graphic>();
        }
        public override void ButtonDownEffect()
        {
            StartChildrenColorTween(colors.pressedColor, false);
        }
        public override void ButtonUpEffect()
        {
            StartChildrenColorTween(colors.normalColor, false);
        }
        public override void ButtonSelectEffect()
        {
            StartChildrenColorTween(_HightLightColor, false);
        }
        public override void ButtonDeselectEffect()
        {
            StartChildrenColorTween(colors.normalColor, false);
        }
        private void StartChildrenColorTween(Color target, bool instant)
        {
            if (_ChildGraphics == null || _ChildGraphics.Length <= 0) return;
            for (var i = 0; i < _ChildGraphics.Length; i++)
            {
                Graphic graphic = _ChildGraphics[i];
                if (graphic == null) continue;
                graphic.CrossFadeColor(target, instant ? 0f : colors.fadeDuration, true, _UseAlpha);
            }
        }
    }



    public class GButtonColorElastic : GButtonColor
    {
        [SerializeField]
        private float _ScaleButtonDown = .7f;
        [SerializeField]
        private float _ScaleButtonUp = 1f;
        [SerializeField]
        private float _ScaleButtonSelect = 1.05f;
        [SerializeField]
        private float _ScaleButtonUnselect = 1f;
        [SerializeField]
        private float _Time = .3f;

        public override void ButtonDownEffect()
        {
            DOTween.Kill(gameObject);
            base.ButtonDownEffect();
            RectTrans.DOScale(Vector3.one * _ScaleButtonDown, _Time).SetUpdate(true);
        }

        protected override void OnDisable()
        {
            DOTween.Kill(gameObject);
            transform.localScale = Vector3.one;
        }

        public override void ButtonUpEffect()
        {
            DOTween.Kill(gameObject);
            base.ButtonUpEffect();
            RectTrans.DOScale(Vector3.one * _ScaleButtonUp, _Time).SetUpdate(true);
        }

        public override void ButtonSelectEffect()
        {
            if (DOTween.IsTweening(gameObject))
            {
                Debug.LogWarning("DOTween isTweening when trigger another DOTween @ButtonSelectEffect");
                return;
            }
            //DOTween.Kill(gameObject);
            base.ButtonSelectEffect();
            RectTrans.DOScale(Vector3.one * _ScaleButtonSelect, .1f).SetUpdate(true);
        }
        public override void ButtonDeselectEffect()
        {
            if (DOTween.IsTweening(gameObject))
            {
                Debug.LogWarning("DOTween isTweening when trigger another DOTween @ButtonDeselectEffect");
                return;
            }
            //DOTween.Kill(gameObject);
            base.ButtonDeselectEffect();
            RectTrans.DOScale(Vector3.one, .1f).SetUpdate(true);
        }
    }


    public class GPolygonButton : GButton
    {
        private Image _Image;

        [Range(0.0f, 0.5f)]
        public float Alpha;

        protected override void Awake()
        {
            _Image = transform.GetComponent<Image>();
            _Image.alphaHitTestMinimumThreshold = Mathf.Max(0f, Alpha);
        }
    }
}

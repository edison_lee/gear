﻿using UnityEngine;
namespace Gear.Runtime.UI
{
public class GListBase : GBase 
{

    public delegate void ListItemDelegate(GameObject list, GameObject item, int index);

    public ListItemDelegate OnInitItem;

    public ListItemDelegate OnSelectItem;

    public ListItemDelegate OnSelectItemButton;
}
}

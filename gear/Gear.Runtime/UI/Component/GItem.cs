using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;
namespace Gear.Runtime.UI
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(GLayout))]
    public class GItem : Selectable, IPointerClickHandler, ISubmitHandler
    {
        [Serializable]
        public class ItemSelectEvent : UnityEvent<bool> { }
        public ItemSelectEvent _OnItemSelected = new ItemSelectEvent();
        private GLayout _Layout;
        private GItemMediater _Mediater;
        private int _Index = -1;
        public int Index
        {
            get { return _Index; }
            set
            {
                if (_Index != value)
                {
                    isOn = false;
                }
                _Index = value;
                gameObject.name = "item-" + _Index;
                SetLayoutPriority();
                _Layout.LayoutChange();
            }
        }
        private bool _IsOn = false;

        public bool isOn
        {
            get { return _IsOn; }
            set
            {
                // if (_IsOn == value) return;
                _IsOn = value;
                if (_OnItemSelected != null)
                {
                    _OnItemSelected.Invoke(_IsOn);
                }

            }
        }
        protected override void Awake()
        {
            GButton[] gbtns = gameObject.GetComponentsInChildren<GButton>(true);
            for (int i = 0; i < gbtns.Length; i++)
            {
                gbtns[i].OnClick += OnItemButtonClick;
            }
            Button[] buttons = gameObject.GetComponentsInChildren<Button>(true);
            for (int i = 0; i < buttons.Length; i++)
            {
                Button btn = buttons[i];
                btn.onClick.AddListener(delegate () { OnItemButtonClick(btn.gameObject); });
            }
            _Layout = GetComponent<GLayout>();
        }

        public void Init(GItemMediater mediater, int index)
        {
            _Mediater = mediater;
            Index = index;
            OnItemInit();
        }
        private void SetLayoutPriority()
        {
            _Layout.Priority = Index;
        }
        private void OnItemInit()
        {
            if (_Mediater._OnItemInit != null)
                _Mediater._OnItemInit(this);
        }
        private void OnItemClick()
        {
            isOn = _Mediater.Unselectable ? !isOn : true;
            if (_Mediater._OnItemSelect != null)
                _Mediater._OnItemSelect(this);
        }
        private void OnItemButtonClick(GameObject button_go)
        {
            if (_Mediater._OnItemButtonClick != null)
                _Mediater._OnItemButtonClick(this, button_go);
        }
        public virtual void OnPointerClick(PointerEventData eventData)
        {
            OnItemClick();
        }
        public virtual void OnSubmit(BaseEventData eventData)
        {
            OnItemClick();
        }
    }
}
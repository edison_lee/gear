﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
namespace Gear.Runtime.UI
{
    public class GText : Text, IPointerClickHandler
    {
        //TODO 考虑数据缓存
        private const string ID_EMOTION = "e";
        private const string ID_LINK = "l";
        private const string TEXT_QUAD = "<quad name={0} size={1} width=1 />";
        private const string TEXT_LINK = "<color={0}>{1}</color>";
        private static readonly Regex m_TextRegex = new Regex(@"\[([el])\](.+?)\[\-\]", RegexOptions.Singleline);
        private List<GTextEmotionModel> m_GraphicItemModels = new List<GTextEmotionModel>();
        private List<GTextLinkModel> m_TextLinkModels = new List<GTextLinkModel>();
        private GTextGraphicBoard m_GraphicBoard;
        private StringBuilder m_Builder = new StringBuilder();
        private string m_OriginText = string.Empty;
        private string m_BuildText = string.Empty;
        private int m_TextID = 0;
        public string OriginText { get { return m_OriginText; } }
        public delegate void OnGTextClick(int textId, int linkId);
        public OnGTextClick OnClick = null;
        public int TextID { set { m_TextID = value; } }

        protected override void Awake()
        {
            this.alignByGeometry = true;
            if (rectTransform.childCount <= 0)
            {
                m_GraphicBoard = new GameObject("_GraphicBoard").AddComponent<GTextGraphicBoard>();
                m_GraphicBoard.rectTransform.SetParent(transform, false);
            }
            else
            {
                m_GraphicBoard = GetComponentInChildren<GTextGraphicBoard>();
                if (m_GraphicBoard == null)
                {
                    m_GraphicBoard = rectTransform.GetChild(0).gameObject.AddComponent<GTextGraphicBoard>();
                }
            }
            FormatBoard();
        }
        public void FormatBoard()
        {
            if (m_GraphicBoard == null) return;
            m_GraphicBoard.rectTransform.anchorMin = rectTransform.anchorMin;
            m_GraphicBoard.rectTransform.anchorMax = rectTransform.anchorMax;
            m_GraphicBoard.rectTransform.pivot = rectTransform.pivot;
        }
        public override void SetVerticesDirty()
        {
            //收集富文本信息
            m_OriginText = m_Text;
            MatchCollection collections = m_TextRegex.Matches(m_OriginText);
            m_Builder = new StringBuilder();
            m_GraphicItemModels.Clear();
            m_TextLinkModels.Clear();
            int last_index = 0;
            int start_index = 0;

            for (var i = 0; i < collections.Count; i++)
            {
                Match match = collections[i];
                int match_index = match.Index;
                string type = match.Groups[1].Value;
                if (type == ID_EMOTION)
                {
                    m_Builder.Append(m_OriginText.Substring(last_index, match_index - last_index));
                    start_index = m_Builder.Length;
                    string quad = string.Format(TEXT_QUAD, match.Groups[2].Value, this.fontSize.ToString());
                    m_Builder.Append(quad);
                    GTextEmotionModel model = new GTextEmotionModel();
                    model.TextIndex = start_index;
                    model.Size = this.fontSize;
                    model.SpriteName = match.Groups[2].Value;
                    m_GraphicItemModels.Add(model);
                }
                else if (type == ID_LINK)
                {
                    string seg = m_OriginText.Substring(last_index, match_index - last_index);
                    m_Builder.Append(seg);
                    start_index = m_Builder.Length;
                    string link = string.Format(TEXT_LINK, "blue", match.Groups[2].Value);
                    m_Builder.Append(link);
                    GTextLinkModel linkModel = new GTextLinkModel();
                    linkModel.StartIndex = start_index;
                    linkModel.EndIndex = m_Builder.Length - 1;
                    linkModel.LinkText = match.Groups[2].Value;
                    m_TextLinkModels.Add(linkModel);
                }
                last_index = match_index + match.Value.Length;
            }
            m_Builder.Append(m_OriginText.Substring(last_index, m_OriginText.Length - last_index));
            m_BuildText = m_Builder.ToString();

            //重绘请求
            base.SetVerticesDirty();
        }
        //这里是富文本的主要逻辑
        //主要分为两部分：1.提取咱们自定义的富文本信息。2.根据提取的信息在做后续处理，包括图文混排和文字链接
        //图文混排： 会根据富文本中提取的信息生成定点位置和uv坐标，最终生成网格，其实相当于一个大Image。
        //文字链接： 根据富文本信息生成文字链接的虚拟点击矩形区域，用来鼠标点击的时候判断是否点到对应的文字。
        //咱们自定义的富文本标记格式为：[标记类型]表情名称or链接文字[-]
        //标记类型：目前只支持两种 一种 e 另一种 l ，e代表表情，l代表文字链接举例：表情 [e]Dog_laugh[-]，链接 [l]www.baidu.com[-]
        static UIVertex vert = new UIVertex();
        protected override void OnPopulateMesh(VertexHelper toFill)
        {
            m_Text = m_BuildText;
            base.OnPopulateMesh(toFill);//生成文本网格
                                        //↓ 根据富文本信息做后续处理
            int i, j, startIndex, indics;
            for (i = 0; i < m_GraphicItemModels.Count; i++)
            {
                GTextEmotionModel model = m_GraphicItemModels[i];
                startIndex = model.TextIndex * 4;

                for (j = 0; j < 4; j++)
                {
                    //清楚乱码
                    indics = startIndex + j;
                    if (indics >= toFill.currentVertCount) continue;
                    toFill.PopulateUIVertex(ref vert, indics);
                    vert.uv0 = vert.uv1 = Vector2.zero;
                    toFill.SetUIVertex(vert, indics);

                    //把quad的定点位置存下来在表情绘制的时候使用
                    //只需要知道对角线的两个定点就能确定这个矩形
                    if (j == 0)
                        model.PosMin = vert.position;
                    if (j == 2)
                        model.PosMax = vert.position;
                }
            }
            m_GraphicBoard.SetVertexData(m_GraphicItemModels);

            for (i = 0; i < m_TextLinkModels.Count; i++)
            {
                GTextLinkModel linkModel = m_TextLinkModels[i];
                startIndex = linkModel.StartIndex * 4;
                int endIndex = linkModel.EndIndex * 4 + 3;
                if (startIndex >= toFill.currentVertCount)
                {
                    continue;
                }
                toFill.PopulateUIVertex(ref vert, startIndex);
                Vector3 pos = vert.position;
                Bounds bounds = new Bounds(pos, Vector3.zero);
                for (j = startIndex; j < endIndex; j++)
                {
                    if (j >= toFill.currentVertCount)
                    {
                        break;
                    }
                    if (j % 2 == 0)
                    {
                        toFill.PopulateUIVertex(ref vert, j);
                        pos = vert.position;
                        if (pos.x < bounds.min.x) // 换行重新添加包围框  
                        {
                            linkModel.AddRect(new Rect(bounds.min, bounds.size));
                            bounds = new Bounds(pos, Vector3.zero);
                        }
                        else
                        {
                            bounds.Encapsulate(pos); // 扩展包围框  
                        }
                    }
                }
                linkModel.AddRect(new Rect(bounds.min, bounds.size));
            }
            m_Text = m_OriginText;
        }
        public void OnPointerClick(PointerEventData eventData)
        {
            if (OnClick == null) return;

            Vector2 lp;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, out lp);
            var clickLinkId = -1;
            for (int i = 0; i < m_TextLinkModels.Count; i++)
            {
                GTextLinkModel linkModel = m_TextLinkModels[i];
                for (int j = 0; j < linkModel.Rects.Count; ++j)
                {
                    if (linkModel.Rects[j].Contains(lp))
                    {
                        clickLinkId = i;
                        break;
                    }
                }
            }
            if (clickLinkId != -1)
                OnClick(m_TextID, clickLinkId + 1);
        }
        public override float preferredWidth
        {
            get
            {
                var settings = GetGenerationSettings(Vector2.zero);
                return cachedTextGeneratorForLayout.GetPreferredWidth(m_BuildText, settings) / pixelsPerUnit;
            }
        }
        public override float preferredHeight
        {
            get
            {
                var settings = GetGenerationSettings(new Vector2(GetPixelAdjustedRect().size.x, 0.0f));
                return cachedTextGeneratorForLayout.GetPreferredHeight(m_BuildText, settings) / pixelsPerUnit;
            }
        }
    }
    public class GTextGraphicBoard : MaskableGraphic
    {
        [NonSerialized]
        private readonly VertexHelper s_VertexHelper = new VertexHelper();
        private IList<GTextEmotionModel> _Models;
        public override Texture mainTexture
        {
            get
            {
                if (GEmotionAssetsManager.Instance.EmotionAssets == null)
                    return s_WhiteTexture;

                if (GEmotionAssetsManager.Instance.EmotionAssets._MainTexture == null)
                    return s_WhiteTexture;
                else
                    return GEmotionAssetsManager.Instance.EmotionAssets._MainTexture;
            }
        }
        public void SetVertexData(List<GTextEmotionModel> models)
        {
            _Models = models;
            DoMeshGeneration();
        }
        private void DoMeshGeneration()
        {
            if (rectTransform != null && rectTransform.rect.width >= 0 && rectTransform.rect.height >= 0)
                OnPopulateMesh(s_VertexHelper);
            else
                s_VertexHelper.Clear();
            s_VertexHelper.FillMesh(workerMesh);
            canvasRenderer.SetMesh(workerMesh);
        }
        static Vector2 uvMin = Vector2.zero;
        static Vector2 uvMax = Vector2.zero;
        protected override void OnPopulateMesh(VertexHelper toFill)
        {
            toFill.Clear();
            if (_Models == null || _Models.Count <= 0) return;
            Color32 color32 = color;

            //TODO lizhixiong
            //在计算定点的时候需要知道Unity的一些默认规则，否则容易陷入泥潭..
            //1.在生成三角片的时候坐标原点是左下角，不是常用的屏幕坐标系（左上角）,UV也一样。
            //2.Unity ugui 默认的三角形的定点绘制顺序如下
            //              0   1 
            //              3   2
            //  索引 ->   012 230
            for (int i = 0; i < _Models.Count; i++)
            {
                GTextEmotionModel model = _Models[i];
                GEmotionAsset asset = model.GetEmotionAsset();
                if (asset != null)
                {
                    Vector4 outerUV = asset.GetOuterUV();
                    uvMin.Set(outerUV.x, outerUV.w);
                    uvMax.Set(outerUV.z, outerUV.y);
                    AddQuad(toFill, model.PosMin, model.PosMax, color32, uvMin, uvMax);
                }
            }
        }
        static void AddQuad(VertexHelper vertexHelper, Vector2 posMin, Vector2 posMax, Color32 color, Vector2 uvMin, Vector2 uvMax)
        {
            int startIndex = vertexHelper.currentVertCount;
            vertexHelper.AddVert(new Vector3(posMin.x, posMin.y, 0), color, new Vector2(uvMin.x, uvMin.y));
            vertexHelper.AddVert(new Vector3(posMin.x, posMax.y, 0), color, new Vector2(uvMin.x, uvMax.y));
            vertexHelper.AddVert(new Vector3(posMax.x, posMax.y, 0), color, new Vector2(uvMax.x, uvMax.y));
            vertexHelper.AddVert(new Vector3(posMax.x, posMin.y, 0), color, new Vector2(uvMax.x, uvMin.y));
            vertexHelper.AddTriangle(startIndex, startIndex + 1, startIndex + 2);
            vertexHelper.AddTriangle(startIndex + 2, startIndex + 3, startIndex);
        }
    }




    /*
     *  图文混排-表情信息类
     */
    public class GTextEmotionModel
    {
        private int _TextIndex;
        private string _SpriteName;
        private int _Size;
        private Vector3 _TextLowerLeftPosition;
        public Vector3 PosMin;
        public Vector3 PosMax;
        public int TextIndex { get { return _TextIndex; } set { _TextIndex = value; } }
        public string SpriteName { get { return _SpriteName; } set { _SpriteName = value; } }
        public int Size { get { return _Size; } set { _Size = value; } }
        public Vector3 TextLowerLeftPosition { get { return _TextLowerLeftPosition; } set { _TextLowerLeftPosition = value; } }
        public GEmotionAsset GetEmotionAsset()
        {
            return GEmotionAssetsManager.Instance.GetEmotionAssetByName(SpriteName);
        }
        public Vector2 GetSizeVector2()
        {
            return Vector2.one * Size;
        }
        public Vector3 GetSizeVector3()
        {
            return new Vector3(Size, Size, 0);
        }
    }


    /*
     *  图文混排-文字链接信息类
     */
    public class GTextLinkModel
    {
        private int _StartIndex;
        private int _EndIndex;
        private string _LinkText;
        public int StartIndex { get { return _StartIndex; } set { _StartIndex = value; } }
        public int EndIndex { get { return _EndIndex; } set { _EndIndex = value; } }
        public string LinkText { get { return _LinkText; } set { _LinkText = value; } }
        private List<Rect> _Rects = new List<Rect>();
        public List<Rect> Rects { get { return _Rects; } }
        public void AddRect(Rect rect)
        {
            _Rects.Add(rect);
        }
    }




    public class GTextEmotionAssets : ScriptableObject
    {
        public Texture2D _MainTexture;
        //所有表情都存到这个list中，如果表情太多可以打成不同的图集，生成到这个list中。
        public List<GEmotionAsset> _EmotionAssets;
        public GEmotionAsset GetEmotionAssetByName(string emotionName)
        {
            GEmotionAsset ast = _EmotionAssets.Find(item => item.Name == emotionName);
            if (ast != null)
            {
                return ast;
            }
            return null;
        }
        public GEmotionAsset GetEmotionAssetById(int id)
        {
            return _EmotionAssets.Find(item => item.Id == id);
        }
    }





    [System.Serializable]
    public class GEmotionAsset
    {
        public int _Id;
        public int Id { get { return _Id; } set { _Id = value; } }
        public Sprite _Sprite;
        public string Name { get { return _Sprite.name; } }
        public Vector2 Pivot { get { return _Sprite.pivot; } }
        public Rect Rect { get { return _Sprite.rect; } }
        public Vector4 GetOuterUV()
        {
            return UnityEngine.Sprites.DataUtility.GetOuterUV(_Sprite);
        }
        public Vector2 GetTextureSize()
        {
            return _Sprite.texture.texelSize;
        }
    }



    public class GEmotionAssetsManager
    {
        //表情资源的命名格式
        //1.表情的命名格式举例：Dog_Laugh,Monkey_Angry,Common_Smile
        //2.拿第一个举例：下划线前面的'Dog'为这个文件的文件名，此时文件夹中应该有两个文件，Dog.png(美术给的)和Dog.asset(工具生成的
        //  选中Dog.png点击生成按钮就会自动生成Dog.asset,名字是一一对应的).下划线后面的'Laugh'为Dog里面的一个单独的小图。
        private const string DefaultFilePath = "Emotions/EAssets";
        private const char Separator = '_';
        private static GEmotionAssetsManager _Instance;
        public static GEmotionAssetsManager Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new GEmotionAssetsManager();
                return _Instance;
            }
        }
        private GTextEmotionAssets _EmotionAssets;
        public GTextEmotionAssets EmotionAssets
        {
            get
            {
                if (_EmotionAssets == null)
                {
                    _EmotionAssets = Resources.Load<GTextEmotionAssets>(DefaultFilePath);
                }
                return _EmotionAssets;
            }
        }

        //获取代表单个表情的GEmotionAsset对象，这个对象存储了单个表情的信息
        public GEmotionAsset GetEmotionAssetByName(string emotionName)
        {
            if (EmotionAssets)
            {
                return EmotionAssets.GetEmotionAssetByName(emotionName);
            }
            return null;
        }
        //获取代表单个表情的GEmotionAsset对象，这个对象存储了单个表情的信息
        public GEmotionAsset GetEmotionAssetById(int id)
        {
            if (EmotionAssets)
            {
                return EmotionAssets.GetEmotionAssetById(id);
            }
            return null;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Events;
using DG.Tweening;
using System;
namespace Gear.Runtime.UI
{
    public class GSimpleList : GBase
    {
        [SerializeField]
        private GameObject _Item;
        [SerializeField]
        private Vector2 _Size;
        [SerializeField]
        private Vector2 _Spacing = Vector2.one * 5;
        [SerializeField]
        private bool _IsSingleSelection = true;
        [SerializeField]
        private RectOffset _Padding = new RectOffset();
        [SerializeField]
        private GridLayoutGroup.Corner _StartCorner = GridLayoutGroup.Corner.UpperLeft;
        [SerializeField]
        protected GridLayoutGroup.Axis _StartAxis = GridLayoutGroup.Axis.Horizontal;
        protected GameObject _Content;
        protected GLayout _LayoutRoot;
        protected List<GItem> _Items = new List<GItem>();
        protected GItemPooller<GItem> _ItemPool = new GItemPooller<GItem>();
        protected int _Count;

        [SerializeField]
        public class ItemClickEvent : UnityEvent<GameObject, GameObject, int> { }
        [SerializeField]
        public class ItemSelectEvent : UnityEvent<GameObject, GameObject, int, bool> { }
        [SerializeField]
        public class ItemButtonClickEvent : UnityEvent<GameObject, GameObject, GameObject, int> { }
        [SerializeField]
        public ItemClickEvent _OnItemInit = new ItemClickEvent();
        [SerializeField]
        public ItemSelectEvent _OnItemSelect = new ItemSelectEvent();
        [SerializeField]
        public ItemButtonClickEvent _OnItemButtonClick = new ItemButtonClickEvent();
        private GItemMediater _ItemMediater;
        private bool _IsAddingItem;
        private bool _IsRemovingItem;

        public bool _NeedTweeningEffect = false;

        private Action _ItemAddCallback;
        private Action _ItemRemoveCallback;
        protected override void Awake()
        {
            _Content = new GameObject("_Content");
            _ItemPool.Regist(_Content, _Item);
            _LayoutRoot = _Content.AddComponent<GLayout>();
            _LayoutRoot.RectTrans.SetParent(RectTrans, false);
            _LayoutRoot.RectTrans.rotation = Quaternion.identity;
            _LayoutRoot.RectTrans.localScale = Vector3.one;
            _LayoutRoot.Size = _Size;
            _LayoutRoot.Spacing = _Spacing;
            _LayoutRoot.Padding = _Padding;
            _LayoutRoot.SortedType = SortType.Priority;
            _LayoutRoot.StartCorner = _StartCorner;
            _LayoutRoot.StartAxis = _StartAxis;
            Vector2 anchor = Vector2.zero;
            if (_StartCorner == GridLayoutGroup.Corner.UpperLeft)
                anchor = Vector2.up;
            else if (_StartCorner == GridLayoutGroup.Corner.UpperRight)
                anchor = Vector2.one;
            else if (_StartCorner == GridLayoutGroup.Corner.LowerRight)
                anchor = Vector2.right;
            else if (_StartCorner == GridLayoutGroup.Corner.LowerLeft)
                anchor = Vector2.zero;
            _LayoutRoot.RectTrans.anchorMax = _LayoutRoot.RectTrans.anchorMin = _LayoutRoot.RectTrans.pivot = anchor;
            if (_Item != null && _Item.activeSelf)
                _Item.SetActive(false);
            _ItemMediater = new GItemMediater(OnItemInit, OnItemSelect, OnItemButtonClick);
            _ItemMediater.Unselectable = false;
        }

        public void SetCount(int count)
        {
            _Count = count;
            for (var i = 0; i < _Items.Count; i++)
            {
                _ItemPool.Release(_Items[i]);
            }
            _Items.Clear();
            for (var i = 0; i < count; i++)
            {
                DoAddItem(i);
            }
            _LayoutRoot.LayoutChange();
        }
        public int GetCount()
        {
            return _Items.Count;
        }
        public void AddItemAddEvent(Action itemAdd)
        {
            _ItemAddCallback = itemAdd;
        }

        public void AddItem(int index)
        {
            if (_IsAddingItem || _IsRemovingItem) return;
            if (index < 0 || index > _Items.Count)
            {
                // Common.HobaDebuger.LogError("GSimpleList :: index out of range.");
                return;
            }
            DoAddItem(index);
        }
        private void DoAddItem(int index)
        {
            if (_NeedTweeningEffect)
            {
                _IsAddingItem = true;
                Tweener ter = null;

                for (int i = index; i < _Items.Count; i++)
                {
                    _Items[i].Index = i + 1;
                    RectTransform rt = _Items[i].GetComponent<RectTransform>();
                    // DOTween.Kill(rt.gameObject);
                    ter = rt.DOAnchorPosY(rt.anchoredPosition.y - rt.sizeDelta.y - _Spacing.y, 0.3f).SetEase(Ease.OutBack);
                }

                Action complete = () =>
                {
                    GItem newItem = _ItemPool.Get();
                    newItem.Init(_ItemMediater, index);
                    _Items.Insert(index, newItem);
                    _LayoutRoot.LayoutChange();
                    RectTransform rect = newItem.GetComponent<RectTransform>();
                // DOTween.Kill(rect.gameObject);
                rect.DOScale(Vector2.zero, 0.3f)
                    .From()
                    .SetEase(Ease.InOutCubic)
                    .OnComplete(() =>
                    {
                        _IsAddingItem = false;
                        if (_ItemAddCallback != null)
                        {
                            _ItemAddCallback();
                            _ItemAddCallback = null;
                        }
                    });
                };

                if (ter != null)
                {
                    ter.OnComplete(() => complete());
                }
                else
                {
                    complete();
                }
            }
            else
            {
                if (index < 0 || index > _Items.Count)
                {
                    Debug.LogErrorFormat("GSimpleList :: index out of range, index = {0}, count = {1}", index, _Items.Count);
                    return;
                }
                GItem newItem = _ItemPool.Get();
                newItem.Init(_ItemMediater, index);
                _Items.Insert(index, newItem);
                for (int i = index + 1, max = _Items.Count; i < max; i++)
                {
                    _Items[i].Index = i;
                }
                _LayoutRoot.LayoutChange();
            }

        }
        public void AddItemRemoveEvent(Action itemRemove)
        {
            _ItemRemoveCallback = itemRemove;
        }
        public void RemoveItem(int index)
        {
            if (_IsAddingItem || _IsRemovingItem) return;
            if (index < 0 || index >= _Items.Count)
            {
                // Common.HobaDebuger.LogError("GSimpleList :: index out of range.");
                return;
            }
            DoRemoveItem(index);
        }
        private void DoRemoveItem(int index)
        {
            _IsRemovingItem = true;
            GItem oldItem = _Items[index];
            RectTransform itemRect = oldItem.GetComponent<RectTransform>();
            Action complete = () =>
            {
                _ItemPool.Release(oldItem);
                _LayoutRoot.LayoutChange();
                itemRect.localScale = Vector3.one;
                _IsRemovingItem = false;
                if (_ItemRemoveCallback != null)
                {
                    _ItemRemoveCallback();
                    _ItemRemoveCallback = null;
                }
            };
            itemRect.DOScale(Vector2.zero, .3f)
            // itemRect.DOAnchorPosX(itemRect.anchoredPosition.x - itemRect.sizeDelta.x, 0.3f)
            .SetEase(Ease.Flash)
            .OnComplete(() =>
            {
                _Items.RemoveAt(index);
                Tweener ter = null;
                for (int i = index; i < _Items.Count; i++)
                {
                    _Items[i].Index = i;
                    RectTransform rt = _Items[i].GetComponent<RectTransform>();
                // DOTween.Kill(rt.gameObject);
                ter = rt.DOAnchorPosY(rt.anchoredPosition.y + rt.sizeDelta.y + _Spacing.y, 0.3f).SetEase(Ease.OutBack);
                }
                if (ter != null)
                {
                    ter.OnComplete(() => complete());
                }
                else
                {
                    complete();
                }
            });
        }

        public GameObject GetItemObject(int index)
        {
            if (index < 0 || index > _Items.Count)
            {
                // Common.HobaDebuger.LogError("GSimpleList :: index out of range.");
                return null;
            }
            GItem item = GetItemByIndex(index);
            if (item)
                return item.gameObject;
            return null;
        }

        public void SetSelectIndex(int index)
        {
            GItem item = GetItemByIndex(index);
            item.isOn = true;
            if (IsActive() && _OnItemSelect != null)
            {
                _OnItemSelect.Invoke(gameObject, item.gameObject, index, item.isOn);
            }
        }

        public void UpdateItem(int index)
        {
            if (index < 0 || index >= _Items.Count)
            {
                // Common.HobaDebuger.LogError("GSimpleList :: index out of range.");
                return;
            }
            _Items[index].Init(_ItemMediater, index);
            _LayoutRoot.LayoutChange();
        }

        public void NotifyOthers(GItem item)
        {
            if (!_IsSingleSelection) return;
            for (int i = 0; i < _Items.Count; i++)
            {
                GItem itm = _Items[i];
                if (itm == item) continue;
                itm.isOn = false;
            }
        }
        private GItem GetItemByIndex(int index)
        {
            if (index < 0 || index >= _Items.Count)
            {
                // Debug.LogError("GSimpleList :: index out of range.");
                return null;
            }
            return _Items[index];
        }

        //the callbacks of items
        private void OnItemInit(GItem item)
        {
            if (_OnItemInit != null)
                _OnItemInit.Invoke(gameObject, item.gameObject, item.Index);
        }
        private void OnItemSelect(GItem item)
        {
            if (_OnItemSelect != null)
                _OnItemSelect.Invoke(gameObject, item.gameObject, item.Index, item.isOn);
            if (item.isOn)
                NotifyOthers(item);
        }
        private void OnItemButtonClick(GItem item, GameObject btn)
        {
            if (_OnItemButtonClick != null)
                _OnItemButtonClick.Invoke(gameObject, item.gameObject, btn, item.Index);
        }
    }
}
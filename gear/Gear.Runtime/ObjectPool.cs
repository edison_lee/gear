//using System;
//using UnityEngine;
//using System.Collections.Generic;

///** 
//    TODO lizhixiong
//    计时清理
// */
//public class ObjectPool<OT> where OT : new()
//{
//    private readonly Stack<OT> m_Pool = new Stack<OT>();
//    // 在池中获取元素后调用
//    private readonly Action<OT> m_OnGet;
//    // 释放元素到池中后调用
//    private readonly Action<OT> m_OnRelease;
//    // 当池中元素为0的时候调用，如果此值为空条用OT类型默认的new创建
//    private readonly Func<OT> m_CreatFunc;
//    // 池中可容纳元素的最大数量
//    private int m_Capacity;
//    // 元素释放后是否定时destroy调
//    private readonly bool m_AutoDestroy;
//    public int Count { get { return m_Pool.Count; } }
//    public int Capacity { get { return m_Capacity; } set { m_Capacity = value; } }
//    public ObjectPool(Action<OT> onGet, Action<OT> onRelease, Func<OT> creatFunc, int capacity = -1, bool autoDestroy = false)
//    {
//        m_OnGet = onGet;
//        m_OnRelease = onRelease;
//        m_CreatFunc = creatFunc;
//        m_Capacity = capacity;
//        m_AutoDestroy = autoDestroy;
//    }

//    public OT Get()
//    {
//        OT element;
//        if (Count == 0)
//        {
//            if (m_CreatFunc != null)
//                element = m_CreatFunc();
//            else
//                element = new OT();
//        }
//        else
//        {
//            element = m_Pool.Pop();
//        }

//        if (element == null)
//        {
//            Debug.LogError("获取了一个空元素！");
//        }

//        if (m_OnGet != null)
//        {
//            m_OnGet(element);
//        }

//        return element;
//    }
//    public void Release(OT element)
//    {
//        if (element == null)
//        {
//            Debug.LogWarning("正在试图释放一个空元素到池中！");
//            return;
//        }

//        if (Capacity > 0 && Count >= Capacity)
//        {
//            Debug.LogWarning("池中元素超出最大限制，无法释放元素！");
//            return;
//        }

//        if (m_Pool.Contains(element))
//        {
//            Debug.LogWarning("正在释放一个已经存在的元素到池中！");
//            return;
//        }

//        m_Pool.Push(element);
//        if (m_OnRelease != null)
//            m_OnRelease(element);
//    }
//}
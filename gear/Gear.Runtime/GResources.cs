﻿
/*****************************************************************************************************************************

    说明：

        Unity文件说明：
            Unity安装包
            Resources目录
                主要存一些初始资源，
                Unity会自动压缩并会被打进游戏安装包中，用Resouces.Load加载。

            streamingAssetPath目录  Unity会原样打进游戏包中，用AssetDatabase.Load加载。
            presistAssetPath目录    于本地缓存目录。

        加载说明：

            资源可存放的位置有4个：1.资源服务器。2.本地缓存。3.游戏包中。4.内存。
            

    Android 本地缓存最大2G
/*****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using UnityEngine;
using Gear.Runtime.Operation;
namespace Gear.Runtime
{
    /// <summary>
    /// 资源管理类
    /// </summary>
    public static class GResources
    {
        private readonly static Dictionary<string, GameObject> m_MemeryCache = new Dictionary<string, GameObject>();
        public static void LoadBundle(string path)
        {
            new GOperationLoadAssetBundle(path, null, result =>
            {
                
            }).Execute();
        }
    }
}

﻿using UnityEngine;
namespace Gear.Runtime
{
    public static class GUtility
    {
        public static void DestroyChildren(Transform transform, bool activeIncluded = true, GameObject exceptObj = null)
        {
            if (transform == null) return;
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                Transform t = transform.GetChild(i);
                if (t == null || t.gameObject == null)
                    continue;
                if (exceptObj != null && t.gameObject == exceptObj)
                    continue;
                if (activeIncluded)
                    Object.DestroyImmediate(t.gameObject);
                else
                {
                    if (t.gameObject.activeSelf)
                    {
                        Object.DestroyImmediate(t.gameObject);
                    }
                }
            }
        }
    }
}

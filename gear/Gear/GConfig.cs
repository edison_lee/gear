﻿using System;
using UnityEngine;
using UnityEditor;
using System.IO;
namespace Gear
{
    public class GConfig : ScriptableObject
    {
        public const string ConfigFileName = "/GearConfigFile.asset";
        private static GConfig m_Instance;
        public static GConfig Instance
        {
            get
            {
                if (m_Instance == null)
                    m_Instance = Resources.Load<GConfig>("GearConfigFile");
                if (m_Instance == null)
                {
                    string dir = Application.dataPath + "/Resources";
                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);
                    string path = dir + ConfigFileName;
                    if (!File.Exists(path))
                    {
                        m_Instance = CreateInstance<GConfig>();
                        AssetDatabase.CreateAsset(m_Instance, "Assets/Resources" + ConfigFileName);
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                    }
                }
                return m_Instance;
            }
        }
        public string AssetBundleSavePath = "/StreamingAssets";
        public string Server = "http://47.92.140.31:8888/";
        public string CdnUrl = "http://localhost";
        public string CodeVersion = "1.0.0";
        public string ResourceVersion = "1.0.0";
        public string[] StandardProjectFolders = {
            "Editor",           // unity内置文件夹  适合存放编辑器脚本。意义：只能访问特定程序集，影响导入顺序                            
            "Scenes",
            "Plugins",          // unity内置文件夹  适合存放三方库，dll，android、ios工程文件等。意义：影响导入顺序
            "Scripts",          // unity内置文件夹  适合存放脚本                    影响导入顺序
            "Shaders",
            "Prefabs",
            "Resources",        // unity内置文件夹  里面的东西会被压缩打到安装包
            "Standard Assets",  // unity内置文件夹  unity制定放原始资源的文件       意义：在于导入顺序
            "Streaming Assets"  // unity内置文件夹  里面的东西会被原样打到安装包
            };

        public string DefaultFontPath = "Assets/Outputs/Interfaces/Fonts/UIFONT.TTF";
        public string RootCanvasPath = "UIRootCanvas";

        private static string DefaultInputPath
        {
            get
            {
                return Application.dataPath + "/../../../docs/tables";
            }
        }
        private static string DefaultOutputCodePath
        {
            get
            {
                return Application.dataPath + "/../../wordslices-runtime/Assets/Scripts/Datas";
            }
        }
        private static string DefaultOutputABPath
        {
            get
            {
                string path = Application.dataPath + "/../../resources";
                if (Application.platform == RuntimePlatform.OSXEditor)
                    path += "/ios";
                if (Application.platform == RuntimePlatform.Android)
                    path += "/android";
                else if (Application.platform == RuntimePlatform.WindowsEditor)
                    path += "/win";
                return path;
            }
        }

        public string InputExcelPath = DefaultInputPath;
        public string OutputCodePath = DefaultOutputCodePath;
        public string OutputABPath = DefaultOutputABPath;
        public string NativeOutputPath = "/Outputs";
        public string OutputCodePathLocal { get { return Application.dataPath + NativeOutputPath + "/Code"; } }
        public string OutputCodePathLocalAbsolute { get { return "Assets" + NativeOutputPath + "/Code"; } }
        public string OutputDataPathLocal { get { return Application.dataPath + NativeOutputPath + "/Data"; } }
        public string OutputDataPathLocalAbsolute { get { return "Assets" + NativeOutputPath + "/Data"; } }
    }
}
